/* method.c generated by valac, the Vala compiler
 * generated from method.vala, do not modify */

/* method.vala
 *
 * Copyright (C) 2008-2011  Florian Brosch
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Florian Brosch <flo.brosch@gmail.com>
 */

#include "valadoc.h"
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <vala.h>
#include <valacodegen.h>
#include <valagee.h>
#include <glib-object.h>

enum  {
	VALADOC_API_METHOD_0_PROPERTY,
	VALADOC_API_METHOD_IMPLICIT_ARRAY_LENGTH_CPARAMETER_NAME_PROPERTY,
	VALADOC_API_METHOD_BASE_METHOD_PROPERTY,
	VALADOC_API_METHOD_RETURN_TYPE_PROPERTY,
	VALADOC_API_METHOD_IS_YIELDS_PROPERTY,
	VALADOC_API_METHOD_IS_ABSTRACT_PROPERTY,
	VALADOC_API_METHOD_IS_VIRTUAL_PROPERTY,
	VALADOC_API_METHOD_IS_OVERRIDE_PROPERTY,
	VALADOC_API_METHOD_IS_STATIC_PROPERTY,
	VALADOC_API_METHOD_IS_CONSTRUCTOR_PROPERTY,
	VALADOC_API_METHOD_IS_INLINE_PROPERTY,
	VALADOC_API_METHOD_IS_DBUS_VISIBLE_PROPERTY,
	VALADOC_API_METHOD_NODE_TYPE_PROPERTY,
	VALADOC_API_METHOD_NUM_PROPERTIES
};
static GParamSpec* valadoc_api_method_properties[VALADOC_API_METHOD_NUM_PROPERTIES];
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _vala_iterable_unref0(var) ((var == NULL) ? NULL : (var = (vala_iterable_unref (var), NULL)))
#define _valadoc_api_signature_builder_unref0(var) ((var == NULL) ? NULL : (var = (valadoc_api_signature_builder_unref (var), NULL)))

struct _ValadocApiMethodPrivate {
	gchar* finish_function_cname;
	gchar* dbus_result_name;
	gchar* dbus_name;
	gchar* cname;
	gchar* _implicit_array_length_cparameter_name;
	ValadocApiMethod* _base_method;
	ValadocApiTypeReference* _return_type;
	gboolean _is_yields;
	gboolean _is_constructor;
	gboolean _is_dbus_visible;
};

static gint ValadocApiMethod_private_offset;
static gpointer valadoc_api_method_parent_class = NULL;
static ValadocApiCallableIface * valadoc_api_method_valadoc_api_callable_parent_iface = NULL;

static void valadoc_api_method_set_is_dbus_visible (ValadocApiMethod* self,
                                             gboolean value);
static void valadoc_api_method_set_is_constructor (ValadocApiMethod* self,
                                            gboolean value);
static void valadoc_api_method_set_is_yields (ValadocApiMethod* self,
                                       gboolean value);
static ValadocContentInline* valadoc_api_method_real_build_signature (ValadocApiItem* base);
static void valadoc_api_method_real_accept (ValadocApiNode* base,
                                     ValadocApiVisitor* visitor);
static void valadoc_api_method_finalize (GObject * obj);
G_GNUC_INTERNAL const gchar* valadoc_api_callable_get_implicit_array_length_cparameter_name (ValadocApiCallable* self);
static void _vala_valadoc_api_method_get_property (GObject * object,
                                            guint property_id,
                                            GValue * value,
                                            GParamSpec * pspec);
G_GNUC_INTERNAL void valadoc_api_callable_set_implicit_array_length_cparameter_name (ValadocApiCallable* self,
                                                                     const gchar* value);
static void _vala_valadoc_api_method_set_property (GObject * object,
                                            guint property_id,
                                            const GValue * value,
                                            GParamSpec * pspec);

static inline gpointer
valadoc_api_method_get_instance_private (ValadocApiMethod* self)
{
	return G_STRUCT_MEMBER_P (self, ValadocApiMethod_private_offset);
}

ValadocApiMethod*
valadoc_api_method_construct (GType object_type,
                              ValadocApiNode* parent,
                              ValadocApiSourceFile* file,
                              const gchar* name,
                              ValaSymbolAccessibility accessibility,
                              ValadocApiSourceComment* comment,
                              ValaMethod* data)
{
	ValadocApiMethod * self = NULL;
	gchar* _tmp0_ = NULL;
	gboolean _tmp1_;
	gboolean _tmp2_;
	gchar* _tmp4_;
	gchar* _tmp5_;
	gchar* _tmp6_;
	gchar* _tmp7_;
	gboolean _tmp8_;
	gboolean _tmp9_;
	g_return_val_if_fail (parent != NULL, NULL);
	g_return_val_if_fail (file != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (data != NULL, NULL);
	self = (ValadocApiMethod*) valadoc_api_symbol_construct (object_type, parent, file, name, accessibility, comment, (ValaSymbol*) data);
	_tmp1_ = vala_method_get_coroutine (data);
	_tmp2_ = _tmp1_;
	if (_tmp2_) {
		gchar* _tmp3_;
		_tmp3_ = vala_get_ccode_finish_name (data);
		_g_free0 (_tmp0_);
		_tmp0_ = _tmp3_;
	} else {
		_g_free0 (_tmp0_);
		_tmp0_ = NULL;
	}
	_tmp4_ = g_strdup (_tmp0_);
	_g_free0 (self->priv->finish_function_cname);
	self->priv->finish_function_cname = _tmp4_;
	_tmp5_ = vala_gd_bus_module_dbus_result_name (data);
	_g_free0 (self->priv->dbus_result_name);
	self->priv->dbus_result_name = _tmp5_;
	_tmp6_ = vala_gd_bus_module_get_dbus_name_for_member ((ValaSymbol*) data);
	_g_free0 (self->priv->dbus_name);
	self->priv->dbus_name = _tmp6_;
	_tmp7_ = vala_get_ccode_name ((ValaCodeNode*) data);
	_g_free0 (self->priv->cname);
	self->priv->cname = _tmp7_;
	valadoc_api_method_set_is_dbus_visible (self, vala_gd_bus_module_is_dbus_visible ((ValaCodeNode*) data));
	valadoc_api_method_set_is_constructor (self, G_TYPE_CHECK_INSTANCE_TYPE (data, VALA_TYPE_CREATION_METHOD));
	_tmp8_ = vala_method_get_coroutine (data);
	_tmp9_ = _tmp8_;
	valadoc_api_method_set_is_yields (self, _tmp9_);
	_g_free0 (_tmp0_);
	return self;
}

ValadocApiMethod*
valadoc_api_method_new (ValadocApiNode* parent,
                        ValadocApiSourceFile* file,
                        const gchar* name,
                        ValaSymbolAccessibility accessibility,
                        ValadocApiSourceComment* comment,
                        ValaMethod* data)
{
	return valadoc_api_method_construct (VALADOC_API_TYPE_METHOD, parent, file, name, accessibility, comment, data);
}

/**
 * Returns the name of this method as it is used in C.
 */
gchar*
valadoc_api_method_get_cname (ValadocApiMethod* self)
{
	const gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->cname;
	_tmp1_ = g_strdup (_tmp0_);
	result = _tmp1_;
	return result;
}

/**
 * Returns the name of the finish function as it is used in C.
 */
gchar*
valadoc_api_method_get_finish_function_cname (ValadocApiMethod* self)
{
	const gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->finish_function_cname;
	_tmp1_ = g_strdup (_tmp0_);
	result = _tmp1_;
	return result;
}

/**
 * Returns the dbus-name.
 */
gchar*
valadoc_api_method_get_dbus_name (ValadocApiMethod* self)
{
	const gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->dbus_name;
	_tmp1_ = g_strdup (_tmp0_);
	result = _tmp1_;
	return result;
}

gchar*
valadoc_api_method_get_dbus_result_name (ValadocApiMethod* self)
{
	const gchar* _tmp0_;
	gchar* _tmp1_;
	gchar* result = NULL;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->dbus_result_name;
	_tmp1_ = g_strdup (_tmp0_);
	result = _tmp1_;
	return result;
}

/**
 * {@inheritDoc}
 */
static gpointer
_vala_iterable_ref0 (gpointer self)
{
	return self ? vala_iterable_ref (self) : NULL;
}

static ValadocContentInline*
valadoc_api_method_real_build_signature (ValadocApiItem* base)
{
	ValadocApiMethod * self;
	ValadocApiSignatureBuilder* signature = NULL;
	ValadocApiSignatureBuilder* _tmp0_;
	ValadocApiSignatureBuilder* _tmp1_;
	ValaSymbolAccessibility _tmp2_;
	ValaSymbolAccessibility _tmp3_;
	const gchar* _tmp4_;
	gboolean _tmp5_;
	gboolean _tmp21_;
	gboolean _tmp23_;
	ValadocApiSignatureBuilder* _tmp29_;
	ValaList* type_parameters = NULL;
	ValaList* _tmp30_;
	ValaList* _tmp31_;
	gint _tmp32_;
	gint _tmp33_;
	ValadocApiSignatureBuilder* _tmp48_;
	gboolean first = FALSE;
	ValadocApiSignatureBuilder* _tmp60_;
	ValaList* exceptions = NULL;
	ValadocApiNodeType* _tmp61_;
	ValadocApiNodeType* _tmp62_;
	gint _tmp62__length1;
	ValaList* _tmp63_;
	ValaList* _tmp64_;
	ValaList* _tmp65_;
	gint _tmp66_;
	gint _tmp67_;
	ValadocApiSignatureBuilder* _tmp79_;
	ValadocContentRun* _tmp80_;
	ValadocContentInline* result = NULL;
	self = (ValadocApiMethod*) base;
	_tmp0_ = valadoc_api_signature_builder_new ();
	signature = _tmp0_;
	_tmp1_ = signature;
	_tmp2_ = valadoc_api_symbol_get_accessibility ((ValadocApiSymbol*) self);
	_tmp3_ = _tmp2_;
	_tmp4_ = vala_symbol_accessibility_to_string (_tmp3_);
	valadoc_api_signature_builder_append_keyword (_tmp1_, _tmp4_, TRUE);
	_tmp5_ = self->priv->_is_constructor;
	if (!_tmp5_) {
		gboolean _tmp6_;
		gboolean _tmp7_;
		gboolean _tmp18_;
		gboolean _tmp19_;
		_tmp6_ = valadoc_api_method_get_is_static (self);
		_tmp7_ = _tmp6_;
		if (_tmp7_) {
			ValadocApiSignatureBuilder* _tmp8_;
			_tmp8_ = signature;
			valadoc_api_signature_builder_append_keyword (_tmp8_, "static", TRUE);
		} else {
			gboolean _tmp9_;
			gboolean _tmp10_;
			_tmp9_ = valadoc_api_method_get_is_abstract (self);
			_tmp10_ = _tmp9_;
			if (_tmp10_) {
				ValadocApiSignatureBuilder* _tmp11_;
				_tmp11_ = signature;
				valadoc_api_signature_builder_append_keyword (_tmp11_, "abstract", TRUE);
			} else {
				gboolean _tmp12_;
				gboolean _tmp13_;
				_tmp12_ = valadoc_api_method_get_is_override (self);
				_tmp13_ = _tmp12_;
				if (_tmp13_) {
					ValadocApiSignatureBuilder* _tmp14_;
					_tmp14_ = signature;
					valadoc_api_signature_builder_append_keyword (_tmp14_, "override", TRUE);
				} else {
					gboolean _tmp15_;
					gboolean _tmp16_;
					_tmp15_ = valadoc_api_method_get_is_virtual (self);
					_tmp16_ = _tmp15_;
					if (_tmp16_) {
						ValadocApiSignatureBuilder* _tmp17_;
						_tmp17_ = signature;
						valadoc_api_signature_builder_append_keyword (_tmp17_, "virtual", TRUE);
					}
				}
			}
		}
		_tmp18_ = valadoc_api_method_get_is_inline (self);
		_tmp19_ = _tmp18_;
		if (_tmp19_) {
			ValadocApiSignatureBuilder* _tmp20_;
			_tmp20_ = signature;
			valadoc_api_signature_builder_append_keyword (_tmp20_, "inline", TRUE);
		}
	}
	_tmp21_ = self->priv->_is_yields;
	if (_tmp21_) {
		ValadocApiSignatureBuilder* _tmp22_;
		_tmp22_ = signature;
		valadoc_api_signature_builder_append_keyword (_tmp22_, "async", TRUE);
	}
	_tmp23_ = self->priv->_is_constructor;
	if (!_tmp23_) {
		ValadocApiSignatureBuilder* _tmp24_;
		ValadocApiTypeReference* _tmp25_;
		ValadocApiTypeReference* _tmp26_;
		ValadocContentInline* _tmp27_;
		ValadocContentInline* _tmp28_;
		_tmp24_ = signature;
		_tmp25_ = valadoc_api_callable_get_return_type ((ValadocApiCallable*) self);
		_tmp26_ = _tmp25_;
		_tmp27_ = valadoc_api_item_get_signature ((ValadocApiItem*) _tmp26_);
		_tmp28_ = _tmp27_;
		valadoc_api_signature_builder_append_content (_tmp24_, _tmp28_, TRUE);
	}
	_tmp29_ = signature;
	valadoc_api_signature_builder_append_symbol (_tmp29_, (ValadocApiNode*) self, TRUE);
	_tmp30_ = valadoc_api_node_get_children_by_type ((ValadocApiNode*) self, VALADOC_API_NODE_TYPE_TYPE_PARAMETER, FALSE);
	type_parameters = _tmp30_;
	_tmp31_ = type_parameters;
	_tmp32_ = vala_collection_get_size ((ValaCollection*) _tmp31_);
	_tmp33_ = _tmp32_;
	if (_tmp33_ > 0) {
		ValadocApiSignatureBuilder* _tmp34_;
		gboolean first = FALSE;
		ValadocApiSignatureBuilder* _tmp47_;
		_tmp34_ = signature;
		valadoc_api_signature_builder_append (_tmp34_, "<", FALSE);
		first = TRUE;
		{
			ValaList* _param_list = NULL;
			ValaList* _tmp35_;
			ValaList* _tmp36_;
			gint _param_size = 0;
			ValaList* _tmp37_;
			gint _tmp38_;
			gint _tmp39_;
			gint _param_index = 0;
			_tmp35_ = type_parameters;
			_tmp36_ = _vala_iterable_ref0 (_tmp35_);
			_param_list = _tmp36_;
			_tmp37_ = _param_list;
			_tmp38_ = vala_collection_get_size ((ValaCollection*) _tmp37_);
			_tmp39_ = _tmp38_;
			_param_size = _tmp39_;
			_param_index = -1;
			while (TRUE) {
				ValadocApiItem* param = NULL;
				ValaList* _tmp40_;
				gpointer _tmp41_;
				ValadocApiSignatureBuilder* _tmp43_;
				ValadocApiItem* _tmp44_;
				ValadocContentInline* _tmp45_;
				ValadocContentInline* _tmp46_;
				_param_index = _param_index + 1;
				if (!(_param_index < _param_size)) {
					break;
				}
				_tmp40_ = _param_list;
				_tmp41_ = vala_list_get (_tmp40_, _param_index);
				param = (ValadocApiItem*) ((ValadocApiNode*) _tmp41_);
				if (!first) {
					ValadocApiSignatureBuilder* _tmp42_;
					_tmp42_ = signature;
					valadoc_api_signature_builder_append (_tmp42_, ",", FALSE);
				}
				_tmp43_ = signature;
				_tmp44_ = param;
				_tmp45_ = valadoc_api_item_get_signature (_tmp44_);
				_tmp46_ = _tmp45_;
				valadoc_api_signature_builder_append_content (_tmp43_, _tmp46_, FALSE);
				first = FALSE;
				_g_object_unref0 (param);
			}
			_vala_iterable_unref0 (_param_list);
		}
		_tmp47_ = signature;
		valadoc_api_signature_builder_append (_tmp47_, ">", FALSE);
	}
	_tmp48_ = signature;
	valadoc_api_signature_builder_append (_tmp48_, "(", TRUE);
	first = TRUE;
	{
		ValaList* _param_list = NULL;
		ValaList* _tmp49_;
		gint _param_size = 0;
		ValaList* _tmp50_;
		gint _tmp51_;
		gint _tmp52_;
		gint _param_index = 0;
		_tmp49_ = valadoc_api_node_get_children_by_type ((ValadocApiNode*) self, VALADOC_API_NODE_TYPE_FORMAL_PARAMETER, FALSE);
		_param_list = _tmp49_;
		_tmp50_ = _param_list;
		_tmp51_ = vala_collection_get_size ((ValaCollection*) _tmp50_);
		_tmp52_ = _tmp51_;
		_param_size = _tmp52_;
		_param_index = -1;
		while (TRUE) {
			ValadocApiNode* param = NULL;
			ValaList* _tmp53_;
			gpointer _tmp54_;
			ValadocApiSignatureBuilder* _tmp56_;
			ValadocApiNode* _tmp57_;
			ValadocContentInline* _tmp58_;
			ValadocContentInline* _tmp59_;
			_param_index = _param_index + 1;
			if (!(_param_index < _param_size)) {
				break;
			}
			_tmp53_ = _param_list;
			_tmp54_ = vala_list_get (_tmp53_, _param_index);
			param = (ValadocApiNode*) _tmp54_;
			if (!first) {
				ValadocApiSignatureBuilder* _tmp55_;
				_tmp55_ = signature;
				valadoc_api_signature_builder_append (_tmp55_, ",", FALSE);
			}
			_tmp56_ = signature;
			_tmp57_ = param;
			_tmp58_ = valadoc_api_item_get_signature ((ValadocApiItem*) _tmp57_);
			_tmp59_ = _tmp58_;
			valadoc_api_signature_builder_append_content (_tmp56_, _tmp59_, !first);
			first = FALSE;
			_g_object_unref0 (param);
		}
		_vala_iterable_unref0 (_param_list);
	}
	_tmp60_ = signature;
	valadoc_api_signature_builder_append (_tmp60_, ")", FALSE);
	_tmp61_ = g_new0 (ValadocApiNodeType, 2);
	_tmp61_[0] = VALADOC_API_NODE_TYPE_ERROR_DOMAIN;
	_tmp61_[1] = VALADOC_API_NODE_TYPE_CLASS;
	_tmp62_ = _tmp61_;
	_tmp62__length1 = 2;
	_tmp63_ = valadoc_api_node_get_children_by_types ((ValadocApiNode*) self, _tmp62_, (gint) 2, TRUE);
	_tmp64_ = _tmp63_;
	_tmp62_ = (g_free (_tmp62_), NULL);
	exceptions = _tmp64_;
	_tmp65_ = exceptions;
	_tmp66_ = vala_collection_get_size ((ValaCollection*) _tmp65_);
	_tmp67_ = _tmp66_;
	if (_tmp67_ > 0) {
		ValadocApiSignatureBuilder* _tmp68_;
		_tmp68_ = signature;
		valadoc_api_signature_builder_append_keyword (_tmp68_, "throws", TRUE);
		first = TRUE;
		{
			ValaList* _param_list = NULL;
			ValaList* _tmp69_;
			ValaList* _tmp70_;
			gint _param_size = 0;
			ValaList* _tmp71_;
			gint _tmp72_;
			gint _tmp73_;
			gint _param_index = 0;
			_tmp69_ = exceptions;
			_tmp70_ = _vala_iterable_ref0 (_tmp69_);
			_param_list = _tmp70_;
			_tmp71_ = _param_list;
			_tmp72_ = vala_collection_get_size ((ValaCollection*) _tmp71_);
			_tmp73_ = _tmp72_;
			_param_size = _tmp73_;
			_param_index = -1;
			while (TRUE) {
				ValadocApiNode* param = NULL;
				ValaList* _tmp74_;
				gpointer _tmp75_;
				ValadocApiSignatureBuilder* _tmp77_;
				ValadocApiNode* _tmp78_;
				_param_index = _param_index + 1;
				if (!(_param_index < _param_size)) {
					break;
				}
				_tmp74_ = _param_list;
				_tmp75_ = vala_list_get (_tmp74_, _param_index);
				param = (ValadocApiNode*) _tmp75_;
				if (!first) {
					ValadocApiSignatureBuilder* _tmp76_;
					_tmp76_ = signature;
					valadoc_api_signature_builder_append (_tmp76_, ",", FALSE);
				}
				_tmp77_ = signature;
				_tmp78_ = param;
				valadoc_api_signature_builder_append_type (_tmp77_, _tmp78_, TRUE);
				first = FALSE;
				_g_object_unref0 (param);
			}
			_vala_iterable_unref0 (_param_list);
		}
	}
	_tmp79_ = signature;
	_tmp80_ = valadoc_api_signature_builder_get (_tmp79_);
	result = (ValadocContentInline*) _tmp80_;
	_vala_iterable_unref0 (exceptions);
	_vala_iterable_unref0 (type_parameters);
	_valadoc_api_signature_builder_unref0 (signature);
	return result;
}

/**
 * {@inheritDoc}
 */
static void
valadoc_api_method_real_accept (ValadocApiNode* base,
                                ValadocApiVisitor* visitor)
{
	ValadocApiMethod * self;
	self = (ValadocApiMethod*) base;
	g_return_if_fail (visitor != NULL);
	valadoc_api_visitor_visit_method (visitor, self);
}

static const gchar*
valadoc_api_method_real_get_implicit_array_length_cparameter_name (ValadocApiCallable* base)
{
	const gchar* result;
	ValadocApiMethod* self;
	const gchar* _tmp0_;
	self = (ValadocApiMethod*) base;
	_tmp0_ = self->priv->_implicit_array_length_cparameter_name;
	result = _tmp0_;
	return result;
}

static void
valadoc_api_method_real_set_implicit_array_length_cparameter_name (ValadocApiCallable* base,
                                                                   const gchar* value)
{
	ValadocApiMethod* self;
	self = (ValadocApiMethod*) base;
	if (g_strcmp0 (value, valadoc_api_method_real_get_implicit_array_length_cparameter_name (base)) != 0) {
		gchar* _tmp0_;
		_tmp0_ = g_strdup (value);
		_g_free0 (self->priv->_implicit_array_length_cparameter_name);
		self->priv->_implicit_array_length_cparameter_name = _tmp0_;
		g_object_notify_by_pspec ((GObject *) self, valadoc_api_method_properties[VALADOC_API_METHOD_IMPLICIT_ARRAY_LENGTH_CPARAMETER_NAME_PROPERTY]);
	}
}

ValadocApiMethod*
valadoc_api_method_get_base_method (ValadocApiMethod* self)
{
	ValadocApiMethod* result;
	ValadocApiMethod* _tmp0_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_base_method;
	result = _tmp0_;
	return result;
}

void
valadoc_api_method_set_base_method (ValadocApiMethod* self,
                                    ValadocApiMethod* value)
{
	g_return_if_fail (self != NULL);
	if (valadoc_api_method_get_base_method (self) != value) {
		self->priv->_base_method = value;
		g_object_notify_by_pspec ((GObject *) self, valadoc_api_method_properties[VALADOC_API_METHOD_BASE_METHOD_PROPERTY]);
	}
}

static ValadocApiTypeReference*
valadoc_api_method_real_get_return_type (ValadocApiCallable* base)
{
	ValadocApiTypeReference* result;
	ValadocApiMethod* self;
	ValadocApiTypeReference* _tmp0_;
	self = (ValadocApiMethod*) base;
	_tmp0_ = self->priv->_return_type;
	result = _tmp0_;
	return result;
}

static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

static void
valadoc_api_method_real_set_return_type (ValadocApiCallable* base,
                                         ValadocApiTypeReference* value)
{
	ValadocApiMethod* self;
	self = (ValadocApiMethod*) base;
	if (valadoc_api_method_real_get_return_type (base) != value) {
		ValadocApiTypeReference* _tmp0_;
		_tmp0_ = _g_object_ref0 (value);
		_g_object_unref0 (self->priv->_return_type);
		self->priv->_return_type = _tmp0_;
		g_object_notify_by_pspec ((GObject *) self, valadoc_api_method_properties[VALADOC_API_METHOD_RETURN_TYPE_PROPERTY]);
	}
}

gboolean
valadoc_api_method_get_is_yields (ValadocApiMethod* self)
{
	gboolean result;
	g_return_val_if_fail (self != NULL, FALSE);
	result = self->priv->_is_yields;
	return result;
}

static void
valadoc_api_method_set_is_yields (ValadocApiMethod* self,
                                  gboolean value)
{
	g_return_if_fail (self != NULL);
	if (valadoc_api_method_get_is_yields (self) != value) {
		self->priv->_is_yields = value;
		g_object_notify_by_pspec ((GObject *) self, valadoc_api_method_properties[VALADOC_API_METHOD_IS_YIELDS_PROPERTY]);
	}
}

gboolean
valadoc_api_method_get_is_abstract (ValadocApiMethod* self)
{
	gboolean result;
	ValaCodeNode* _tmp0_;
	ValaCodeNode* _tmp1_;
	gboolean _tmp2_;
	gboolean _tmp3_;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = valadoc_api_item_get_data ((ValadocApiItem*) self);
	_tmp1_ = _tmp0_;
	_tmp2_ = vala_method_get_is_abstract (G_TYPE_CHECK_INSTANCE_CAST (_tmp1_, VALA_TYPE_METHOD, ValaMethod));
	_tmp3_ = _tmp2_;
	result = _tmp3_;
	return result;
}

gboolean
valadoc_api_method_get_is_virtual (ValadocApiMethod* self)
{
	gboolean result;
	ValaCodeNode* _tmp0_;
	ValaCodeNode* _tmp1_;
	gboolean _tmp2_;
	gboolean _tmp3_;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = valadoc_api_item_get_data ((ValadocApiItem*) self);
	_tmp1_ = _tmp0_;
	_tmp2_ = vala_method_get_is_virtual (G_TYPE_CHECK_INSTANCE_CAST (_tmp1_, VALA_TYPE_METHOD, ValaMethod));
	_tmp3_ = _tmp2_;
	result = _tmp3_;
	return result;
}

gboolean
valadoc_api_method_get_is_override (ValadocApiMethod* self)
{
	gboolean result;
	ValaCodeNode* _tmp0_;
	ValaCodeNode* _tmp1_;
	gboolean _tmp2_;
	gboolean _tmp3_;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = valadoc_api_item_get_data ((ValadocApiItem*) self);
	_tmp1_ = _tmp0_;
	_tmp2_ = vala_method_get_overrides (G_TYPE_CHECK_INSTANCE_CAST (_tmp1_, VALA_TYPE_METHOD, ValaMethod));
	_tmp3_ = _tmp2_;
	result = _tmp3_;
	return result;
}

gboolean
valadoc_api_method_get_is_static (ValadocApiMethod* self)
{
	gboolean result;
	gboolean _tmp0_ = FALSE;
	gboolean _tmp1_ = FALSE;
	gboolean _tmp2_;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp2_ = self->priv->_is_constructor;
	if (!_tmp2_) {
		ValaCodeNode* _tmp3_;
		ValaCodeNode* _tmp4_;
		ValaMemberBinding _tmp5_;
		ValaMemberBinding _tmp6_;
		_tmp3_ = valadoc_api_item_get_data ((ValadocApiItem*) self);
		_tmp4_ = _tmp3_;
		_tmp5_ = vala_method_get_binding (G_TYPE_CHECK_INSTANCE_CAST (_tmp4_, VALA_TYPE_METHOD, ValaMethod));
		_tmp6_ = _tmp5_;
		_tmp1_ = _tmp6_ == VALA_MEMBER_BINDING_STATIC;
	} else {
		_tmp1_ = FALSE;
	}
	if (_tmp1_) {
		ValadocApiItem* _tmp7_;
		ValadocApiItem* _tmp8_;
		_tmp7_ = valadoc_api_item_get_parent ((ValadocApiItem*) self);
		_tmp8_ = _tmp7_;
		_tmp0_ = VALADOC_API_IS_NAMESPACE (_tmp8_) == FALSE;
	} else {
		_tmp0_ = FALSE;
	}
	result = _tmp0_;
	return result;
}

gboolean
valadoc_api_method_get_is_constructor (ValadocApiMethod* self)
{
	gboolean result;
	g_return_val_if_fail (self != NULL, FALSE);
	result = self->priv->_is_constructor;
	return result;
}

static void
valadoc_api_method_set_is_constructor (ValadocApiMethod* self,
                                       gboolean value)
{
	g_return_if_fail (self != NULL);
	if (valadoc_api_method_get_is_constructor (self) != value) {
		self->priv->_is_constructor = value;
		g_object_notify_by_pspec ((GObject *) self, valadoc_api_method_properties[VALADOC_API_METHOD_IS_CONSTRUCTOR_PROPERTY]);
	}
}

gboolean
valadoc_api_method_get_is_inline (ValadocApiMethod* self)
{
	gboolean result;
	ValaCodeNode* _tmp0_;
	ValaCodeNode* _tmp1_;
	gboolean _tmp2_;
	gboolean _tmp3_;
	g_return_val_if_fail (self != NULL, FALSE);
	_tmp0_ = valadoc_api_item_get_data ((ValadocApiItem*) self);
	_tmp1_ = _tmp0_;
	_tmp2_ = vala_method_get_is_inline (G_TYPE_CHECK_INSTANCE_CAST (_tmp1_, VALA_TYPE_METHOD, ValaMethod));
	_tmp3_ = _tmp2_;
	result = _tmp3_;
	return result;
}

gboolean
valadoc_api_method_get_is_dbus_visible (ValadocApiMethod* self)
{
	gboolean result;
	g_return_val_if_fail (self != NULL, FALSE);
	result = self->priv->_is_dbus_visible;
	return result;
}

static void
valadoc_api_method_set_is_dbus_visible (ValadocApiMethod* self,
                                        gboolean value)
{
	g_return_if_fail (self != NULL);
	if (valadoc_api_method_get_is_dbus_visible (self) != value) {
		self->priv->_is_dbus_visible = value;
		g_object_notify_by_pspec ((GObject *) self, valadoc_api_method_properties[VALADOC_API_METHOD_IS_DBUS_VISIBLE_PROPERTY]);
	}
}

static ValadocApiNodeType
valadoc_api_method_real_get_node_type (ValadocApiNode* base)
{
	ValadocApiNodeType result;
	ValadocApiMethod* self;
	ValadocApiNodeType _tmp0_ = 0;
	gboolean _tmp1_;
	self = (ValadocApiMethod*) base;
	_tmp1_ = self->priv->_is_constructor;
	if (_tmp1_) {
		_tmp0_ = VALADOC_API_NODE_TYPE_CREATION_METHOD;
	} else {
		ValadocApiNodeType _tmp2_ = 0;
		gboolean _tmp3_;
		gboolean _tmp4_;
		_tmp3_ = valadoc_api_method_get_is_static (self);
		_tmp4_ = _tmp3_;
		if (_tmp4_) {
			_tmp2_ = VALADOC_API_NODE_TYPE_STATIC_METHOD;
		} else {
			_tmp2_ = VALADOC_API_NODE_TYPE_METHOD;
		}
		_tmp0_ = _tmp2_;
	}
	result = _tmp0_;
	return result;
}

static void
valadoc_api_method_class_init (ValadocApiMethodClass * klass,
                               gpointer klass_data)
{
	valadoc_api_method_parent_class = g_type_class_peek_parent (klass);
	g_type_class_adjust_private_offset (klass, &ValadocApiMethod_private_offset);
	((ValadocApiItemClass *) klass)->build_signature = (ValadocContentInline* (*) (ValadocApiItem*)) valadoc_api_method_real_build_signature;
	((ValadocApiNodeClass *) klass)->accept = (void (*) (ValadocApiNode*, ValadocApiVisitor*)) valadoc_api_method_real_accept;
	VALADOC_API_NODE_CLASS (klass)->get_node_type = valadoc_api_method_real_get_node_type;
	G_OBJECT_CLASS (klass)->get_property = _vala_valadoc_api_method_get_property;
	G_OBJECT_CLASS (klass)->set_property = _vala_valadoc_api_method_set_property;
	G_OBJECT_CLASS (klass)->finalize = valadoc_api_method_finalize;
	/**
	 * {@inheritDoc}
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IMPLICIT_ARRAY_LENGTH_CPARAMETER_NAME_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IMPLICIT_ARRAY_LENGTH_CPARAMETER_NAME_PROPERTY] = g_param_spec_string ("implicit-array-length-cparameter-name", "implicit-array-length-cparameter-name", "implicit-array-length-cparameter-name", NULL, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
	/**
	 * Specifies the virtual or abstract method this method overrides.
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_BASE_METHOD_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_BASE_METHOD_PROPERTY] = g_param_spec_object ("base-method", "base-method", "base-method", VALADOC_API_TYPE_METHOD, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
	/**
	 * {@inheritDoc}
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_RETURN_TYPE_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_RETURN_TYPE_PROPERTY] = g_param_spec_object ("return-type", "return-type", "return-type", VALADOC_API_TYPE_TYPEREFERENCE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE | G_PARAM_WRITABLE));
	/**
	 * Specifies whether this method is asynchronous
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_YIELDS_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_YIELDS_PROPERTY] = g_param_spec_boolean ("is-yields", "is-yields", "is-yields", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * Specifies whether this method is abstract
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_ABSTRACT_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_ABSTRACT_PROPERTY] = g_param_spec_boolean ("is-abstract", "is-abstract", "is-abstract", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * Specifies whether this method is virtual
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_VIRTUAL_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_VIRTUAL_PROPERTY] = g_param_spec_boolean ("is-virtual", "is-virtual", "is-virtual", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * Specifies whether this method overrides another one
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_OVERRIDE_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_OVERRIDE_PROPERTY] = g_param_spec_boolean ("is-override", "is-override", "is-override", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * Specifies whether this method is static
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_STATIC_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_STATIC_PROPERTY] = g_param_spec_boolean ("is-static", "is-static", "is-static", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * Specifies whether this method is a creation method
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_CONSTRUCTOR_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_CONSTRUCTOR_PROPERTY] = g_param_spec_boolean ("is-constructor", "is-constructor", "is-constructor", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * Specifies whether this method is inline
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_INLINE_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_INLINE_PROPERTY] = g_param_spec_boolean ("is-inline", "is-inline", "is-inline", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * Specifies whether this method is visible for dbus
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_IS_DBUS_VISIBLE_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_IS_DBUS_VISIBLE_PROPERTY] = g_param_spec_boolean ("is-dbus-visible", "is-dbus-visible", "is-dbus-visible", FALSE, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
	/**
	 * {@inheritDoc}
	 */
	g_object_class_install_property (G_OBJECT_CLASS (klass), VALADOC_API_METHOD_NODE_TYPE_PROPERTY, valadoc_api_method_properties[VALADOC_API_METHOD_NODE_TYPE_PROPERTY] = g_param_spec_enum ("node-type", "node-type", "node-type", VALADOC_API_TYPE_NODE_TYPE, 0, G_PARAM_STATIC_STRINGS | G_PARAM_READABLE));
}

static void
valadoc_api_method_valadoc_api_callable_interface_init (ValadocApiCallableIface * iface,
                                                        gpointer iface_data)
{
	valadoc_api_method_valadoc_api_callable_parent_iface = g_type_interface_peek_parent (iface);
	iface->get_implicit_array_length_cparameter_name = valadoc_api_method_real_get_implicit_array_length_cparameter_name;
	iface->set_implicit_array_length_cparameter_name = valadoc_api_method_real_set_implicit_array_length_cparameter_name;
	iface->get_return_type = valadoc_api_method_real_get_return_type;
	iface->set_return_type = valadoc_api_method_real_set_return_type;
}

static void
valadoc_api_method_instance_init (ValadocApiMethod * self,
                                  gpointer klass)
{
	self->priv = valadoc_api_method_get_instance_private (self);
}

static void
valadoc_api_method_finalize (GObject * obj)
{
	ValadocApiMethod * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALADOC_API_TYPE_METHOD, ValadocApiMethod);
	_g_free0 (self->priv->finish_function_cname);
	_g_free0 (self->priv->dbus_result_name);
	_g_free0 (self->priv->dbus_name);
	_g_free0 (self->priv->cname);
	_g_free0 (self->priv->_implicit_array_length_cparameter_name);
	_g_object_unref0 (self->priv->_return_type);
	G_OBJECT_CLASS (valadoc_api_method_parent_class)->finalize (obj);
}

/**
 * Represents a function or a method.
 */
GType
valadoc_api_method_get_type (void)
{
	static volatile gsize valadoc_api_method_type_id__volatile = 0;
	if (g_once_init_enter (&valadoc_api_method_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ValadocApiMethodClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) valadoc_api_method_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ValadocApiMethod), 0, (GInstanceInitFunc) valadoc_api_method_instance_init, NULL };
		static const GInterfaceInfo valadoc_api_callable_info = { (GInterfaceInitFunc) valadoc_api_method_valadoc_api_callable_interface_init, (GInterfaceFinalizeFunc) NULL, NULL};
		GType valadoc_api_method_type_id;
		valadoc_api_method_type_id = g_type_register_static (VALADOC_API_TYPE_SYMBOL, "ValadocApiMethod", &g_define_type_info, 0);
		g_type_add_interface_static (valadoc_api_method_type_id, VALADOC_API_TYPE_CALLABLE, &valadoc_api_callable_info);
		ValadocApiMethod_private_offset = g_type_add_instance_private (valadoc_api_method_type_id, sizeof (ValadocApiMethodPrivate));
		g_once_init_leave (&valadoc_api_method_type_id__volatile, valadoc_api_method_type_id);
	}
	return valadoc_api_method_type_id__volatile;
}

static void
_vala_valadoc_api_method_get_property (GObject * object,
                                       guint property_id,
                                       GValue * value,
                                       GParamSpec * pspec)
{
	ValadocApiMethod * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (object, VALADOC_API_TYPE_METHOD, ValadocApiMethod);
	switch (property_id) {
		case VALADOC_API_METHOD_IMPLICIT_ARRAY_LENGTH_CPARAMETER_NAME_PROPERTY:
		g_value_set_string (value, valadoc_api_callable_get_implicit_array_length_cparameter_name ((ValadocApiCallable*) self));
		break;
		case VALADOC_API_METHOD_BASE_METHOD_PROPERTY:
		g_value_set_object (value, valadoc_api_method_get_base_method (self));
		break;
		case VALADOC_API_METHOD_RETURN_TYPE_PROPERTY:
		g_value_set_object (value, valadoc_api_callable_get_return_type ((ValadocApiCallable*) self));
		break;
		case VALADOC_API_METHOD_IS_YIELDS_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_yields (self));
		break;
		case VALADOC_API_METHOD_IS_ABSTRACT_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_abstract (self));
		break;
		case VALADOC_API_METHOD_IS_VIRTUAL_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_virtual (self));
		break;
		case VALADOC_API_METHOD_IS_OVERRIDE_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_override (self));
		break;
		case VALADOC_API_METHOD_IS_STATIC_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_static (self));
		break;
		case VALADOC_API_METHOD_IS_CONSTRUCTOR_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_constructor (self));
		break;
		case VALADOC_API_METHOD_IS_INLINE_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_inline (self));
		break;
		case VALADOC_API_METHOD_IS_DBUS_VISIBLE_PROPERTY:
		g_value_set_boolean (value, valadoc_api_method_get_is_dbus_visible (self));
		break;
		case VALADOC_API_METHOD_NODE_TYPE_PROPERTY:
		g_value_set_enum (value, valadoc_api_node_get_node_type ((ValadocApiNode*) self));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
_vala_valadoc_api_method_set_property (GObject * object,
                                       guint property_id,
                                       const GValue * value,
                                       GParamSpec * pspec)
{
	ValadocApiMethod * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (object, VALADOC_API_TYPE_METHOD, ValadocApiMethod);
	switch (property_id) {
		case VALADOC_API_METHOD_IMPLICIT_ARRAY_LENGTH_CPARAMETER_NAME_PROPERTY:
		valadoc_api_callable_set_implicit_array_length_cparameter_name ((ValadocApiCallable*) self, g_value_get_string (value));
		break;
		case VALADOC_API_METHOD_BASE_METHOD_PROPERTY:
		valadoc_api_method_set_base_method (self, g_value_get_object (value));
		break;
		case VALADOC_API_METHOD_RETURN_TYPE_PROPERTY:
		valadoc_api_callable_set_return_type ((ValadocApiCallable*) self, g_value_get_object (value));
		break;
		case VALADOC_API_METHOD_IS_YIELDS_PROPERTY:
		valadoc_api_method_set_is_yields (self, g_value_get_boolean (value));
		break;
		case VALADOC_API_METHOD_IS_CONSTRUCTOR_PROPERTY:
		valadoc_api_method_set_is_constructor (self, g_value_get_boolean (value));
		break;
		case VALADOC_API_METHOD_IS_DBUS_VISIBLE_PROPERTY:
		valadoc_api_method_set_is_dbus_visible (self, g_value_get_boolean (value));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

