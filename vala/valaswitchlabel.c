/* valaswitchlabel.c generated by valac, the Vala compiler
 * generated from valaswitchlabel.vala, do not modify */

/* valaswitchlabel.vala
 *
 * Copyright (C) 2006-2010  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

#include "vala.h"
#include <glib.h>
#include <valagee.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>

#define _vala_code_node_unref0(var) ((var == NULL) ? NULL : (var = (vala_code_node_unref (var), NULL)))
#define _g_free0(var) (var = (g_free (var), NULL))
#define _vala_iterable_unref0(var) ((var == NULL) ? NULL : (var = (vala_iterable_unref (var), NULL)))

struct _ValaSwitchLabelPrivate {
	ValaExpression* _expression;
};

static gint ValaSwitchLabel_private_offset;
static gpointer vala_switch_label_parent_class = NULL;

static void vala_switch_label_real_accept (ValaCodeNode* base,
                                    ValaCodeVisitor* visitor);
static void vala_switch_label_real_accept_children (ValaCodeNode* base,
                                             ValaCodeVisitor* visitor);
static gboolean vala_switch_label_real_check (ValaCodeNode* base,
                                       ValaCodeContext* context);
static void vala_switch_label_real_emit (ValaCodeNode* base,
                                  ValaCodeGenerator* codegen);
static void vala_switch_label_finalize (ValaCodeNode * obj);

static inline gpointer
vala_switch_label_get_instance_private (ValaSwitchLabel* self)
{
	return G_STRUCT_MEMBER_P (self, ValaSwitchLabel_private_offset);
}

/**
 * Creates a new switch case label.
 *
 * @param expr   label expression
 * @param source reference to source code
 * @return       newly created switch case label
 */
ValaSwitchLabel*
vala_switch_label_construct (GType object_type,
                             ValaExpression* expr,
                             ValaSourceReference* source)
{
	ValaSwitchLabel* self = NULL;
	g_return_val_if_fail (expr != NULL, NULL);
	self = (ValaSwitchLabel*) vala_code_node_construct (object_type);
	vala_switch_label_set_expression (self, expr);
	vala_code_node_set_source_reference ((ValaCodeNode*) self, source);
	return self;
}

ValaSwitchLabel*
vala_switch_label_new (ValaExpression* expr,
                       ValaSourceReference* source)
{
	return vala_switch_label_construct (VALA_TYPE_SWITCH_LABEL, expr, source);
}

/**
 * Creates a new switch default label.
 *
 * @param source reference to source code
 * @return       newly created switch default label
 */
ValaSwitchLabel*
vala_switch_label_construct_with_default (GType object_type,
                                          ValaSourceReference* source)
{
	ValaSwitchLabel* self = NULL;
	self = (ValaSwitchLabel*) vala_code_node_construct (object_type);
	vala_code_node_set_source_reference ((ValaCodeNode*) self, source);
	return self;
}

ValaSwitchLabel*
vala_switch_label_new_with_default (ValaSourceReference* source)
{
	return vala_switch_label_construct_with_default (VALA_TYPE_SWITCH_LABEL, source);
}

static void
vala_switch_label_real_accept (ValaCodeNode* base,
                               ValaCodeVisitor* visitor)
{
	ValaSwitchLabel * self;
	self = (ValaSwitchLabel*) base;
	g_return_if_fail (visitor != NULL);
	vala_code_visitor_visit_switch_label (visitor, self);
}

static void
vala_switch_label_real_accept_children (ValaCodeNode* base,
                                        ValaCodeVisitor* visitor)
{
	ValaSwitchLabel * self;
	ValaExpression* _tmp0_;
	ValaExpression* _tmp1_;
	self = (ValaSwitchLabel*) base;
	g_return_if_fail (visitor != NULL);
	_tmp0_ = vala_switch_label_get_expression (self);
	_tmp1_ = _tmp0_;
	if (_tmp1_ != NULL) {
		ValaExpression* _tmp2_;
		ValaExpression* _tmp3_;
		ValaExpression* _tmp4_;
		ValaExpression* _tmp5_;
		_tmp2_ = vala_switch_label_get_expression (self);
		_tmp3_ = _tmp2_;
		vala_code_node_accept ((ValaCodeNode*) _tmp3_, visitor);
		_tmp4_ = vala_switch_label_get_expression (self);
		_tmp5_ = _tmp4_;
		vala_code_visitor_visit_end_full_expression (visitor, _tmp5_);
	}
}

static gpointer
_vala_code_node_ref0 (gpointer self)
{
	return self ? vala_code_node_ref (self) : NULL;
}

static gboolean
vala_switch_label_real_check (ValaCodeNode* base,
                              ValaCodeContext* context)
{
	ValaSwitchLabel * self;
	gboolean _tmp0_;
	gboolean _tmp1_;
	ValaExpression* _tmp4_;
	ValaExpression* _tmp5_;
	gboolean result = FALSE;
	self = (ValaSwitchLabel*) base;
	g_return_val_if_fail (context != NULL, FALSE);
	_tmp0_ = vala_code_node_get_checked ((ValaCodeNode*) self);
	_tmp1_ = _tmp0_;
	if (_tmp1_) {
		gboolean _tmp2_;
		gboolean _tmp3_;
		_tmp2_ = vala_code_node_get_error ((ValaCodeNode*) self);
		_tmp3_ = _tmp2_;
		result = !_tmp3_;
		return result;
	}
	vala_code_node_set_checked ((ValaCodeNode*) self, TRUE);
	_tmp4_ = vala_switch_label_get_expression (self);
	_tmp5_ = _tmp4_;
	if (_tmp5_ != NULL) {
		ValaSwitchStatement* switch_statement = NULL;
		ValaSwitchSection* _tmp6_;
		ValaSwitchSection* _tmp7_;
		ValaCodeNode* _tmp8_;
		ValaCodeNode* _tmp9_;
		ValaSwitchStatement* _tmp10_;
		ValaDataType* condition_target_type = NULL;
		ValaSwitchStatement* _tmp11_;
		ValaExpression* _tmp12_;
		ValaExpression* _tmp13_;
		ValaDataType* _tmp14_;
		ValaDataType* _tmp15_;
		ValaDataType* _tmp16_;
		gboolean _tmp17_ = FALSE;
		gboolean _tmp18_ = FALSE;
		ValaExpression* _tmp19_;
		ValaExpression* _tmp20_;
		ValaSymbol* _tmp21_;
		ValaSymbol* _tmp22_;
		ValaExpression* _tmp54_;
		ValaExpression* _tmp55_;
		ValaExpression* _tmp56_;
		ValaExpression* _tmp57_;
		ValaExpression* _tmp62_;
		ValaExpression* _tmp63_;
		ValaDataType* _tmp64_;
		ValaDataType* _tmp65_;
		ValaSwitchStatement* _tmp66_;
		ValaExpression* _tmp67_;
		ValaExpression* _tmp68_;
		ValaDataType* _tmp69_;
		ValaDataType* _tmp70_;
		_tmp6_ = vala_switch_label_get_section (self);
		_tmp7_ = _tmp6_;
		_tmp8_ = vala_code_node_get_parent_node ((ValaCodeNode*) _tmp7_);
		_tmp9_ = _tmp8_;
		_tmp10_ = _vala_code_node_ref0 (G_TYPE_CHECK_INSTANCE_CAST (_tmp9_, VALA_TYPE_SWITCH_STATEMENT, ValaSwitchStatement));
		switch_statement = _tmp10_;
		_tmp11_ = switch_statement;
		_tmp12_ = vala_switch_statement_get_expression (_tmp11_);
		_tmp13_ = _tmp12_;
		_tmp14_ = vala_expression_get_target_type (_tmp13_);
		_tmp15_ = _tmp14_;
		_tmp16_ = _vala_code_node_ref0 (_tmp15_);
		condition_target_type = _tmp16_;
		_tmp19_ = vala_switch_label_get_expression (self);
		_tmp20_ = _tmp19_;
		_tmp21_ = vala_expression_get_symbol_reference (_tmp20_);
		_tmp22_ = _tmp21_;
		if (_tmp22_ == NULL) {
			ValaDataType* _tmp23_;
			_tmp23_ = condition_target_type;
			_tmp18_ = _tmp23_ != NULL;
		} else {
			_tmp18_ = FALSE;
		}
		if (_tmp18_) {
			ValaDataType* _tmp24_;
			ValaTypeSymbol* _tmp25_;
			ValaTypeSymbol* _tmp26_;
			_tmp24_ = condition_target_type;
			_tmp25_ = vala_data_type_get_data_type (_tmp24_);
			_tmp26_ = _tmp25_;
			_tmp17_ = VALA_IS_ENUM (_tmp26_);
		} else {
			_tmp17_ = FALSE;
		}
		if (_tmp17_) {
			ValaEnum* enum_type = NULL;
			ValaDataType* _tmp27_;
			ValaTypeSymbol* _tmp28_;
			ValaTypeSymbol* _tmp29_;
			ValaEnum* _tmp30_;
			_tmp27_ = condition_target_type;
			_tmp28_ = vala_data_type_get_data_type (_tmp27_);
			_tmp29_ = _tmp28_;
			_tmp30_ = _vala_code_node_ref0 (G_TYPE_CHECK_INSTANCE_CAST (_tmp29_, VALA_TYPE_ENUM, ValaEnum));
			enum_type = _tmp30_;
			{
				ValaList* _val_list = NULL;
				ValaEnum* _tmp31_;
				ValaList* _tmp32_;
				gint _val_size = 0;
				ValaList* _tmp33_;
				gint _tmp34_;
				gint _tmp35_;
				gint _val_index = 0;
				_tmp31_ = enum_type;
				_tmp32_ = vala_enum_get_values (_tmp31_);
				_val_list = _tmp32_;
				_tmp33_ = _val_list;
				_tmp34_ = vala_collection_get_size ((ValaCollection*) _tmp33_);
				_tmp35_ = _tmp34_;
				_val_size = _tmp35_;
				_val_index = -1;
				while (TRUE) {
					ValaEnumValue* val = NULL;
					ValaList* _tmp36_;
					gpointer _tmp37_;
					ValaExpression* _tmp38_;
					ValaExpression* _tmp39_;
					gchar* _tmp40_;
					gchar* _tmp41_;
					ValaEnumValue* _tmp42_;
					const gchar* _tmp43_;
					const gchar* _tmp44_;
					gboolean _tmp45_;
					_val_index = _val_index + 1;
					if (!(_val_index < _val_size)) {
						break;
					}
					_tmp36_ = _val_list;
					_tmp37_ = vala_list_get (_tmp36_, _val_index);
					val = (ValaEnumValue*) _tmp37_;
					_tmp38_ = vala_switch_label_get_expression (self);
					_tmp39_ = _tmp38_;
					_tmp40_ = vala_code_node_to_string ((ValaCodeNode*) _tmp39_);
					_tmp41_ = _tmp40_;
					_tmp42_ = val;
					_tmp43_ = vala_symbol_get_name ((ValaSymbol*) _tmp42_);
					_tmp44_ = _tmp43_;
					_tmp45_ = g_strcmp0 (_tmp41_, _tmp44_) == 0;
					_g_free0 (_tmp41_);
					if (_tmp45_) {
						ValaExpression* _tmp46_;
						ValaExpression* _tmp47_;
						ValaDataType* _tmp48_;
						ValaDataType* _tmp49_;
						ValaDataType* _tmp50_;
						ValaExpression* _tmp51_;
						ValaExpression* _tmp52_;
						ValaEnumValue* _tmp53_;
						_tmp46_ = vala_switch_label_get_expression (self);
						_tmp47_ = _tmp46_;
						_tmp48_ = condition_target_type;
						_tmp49_ = vala_data_type_copy (_tmp48_);
						_tmp50_ = _tmp49_;
						vala_expression_set_target_type (_tmp47_, _tmp50_);
						_vala_code_node_unref0 (_tmp50_);
						_tmp51_ = vala_switch_label_get_expression (self);
						_tmp52_ = _tmp51_;
						_tmp53_ = val;
						vala_expression_set_symbol_reference (_tmp52_, (ValaSymbol*) _tmp53_);
						_vala_code_node_unref0 (val);
						break;
					}
					_vala_code_node_unref0 (val);
				}
				_vala_iterable_unref0 (_val_list);
			}
			_vala_code_node_unref0 (enum_type);
		}
		_tmp54_ = vala_switch_label_get_expression (self);
		_tmp55_ = _tmp54_;
		if (!vala_code_node_check ((ValaCodeNode*) _tmp55_, context)) {
			vala_code_node_set_error ((ValaCodeNode*) self, TRUE);
			result = FALSE;
			_vala_code_node_unref0 (condition_target_type);
			_vala_code_node_unref0 (switch_statement);
			return result;
		}
		_tmp56_ = vala_switch_label_get_expression (self);
		_tmp57_ = _tmp56_;
		if (!vala_expression_is_constant (_tmp57_)) {
			ValaExpression* _tmp58_;
			ValaExpression* _tmp59_;
			ValaSourceReference* _tmp60_;
			ValaSourceReference* _tmp61_;
			vala_code_node_set_error ((ValaCodeNode*) self, TRUE);
			_tmp58_ = vala_switch_label_get_expression (self);
			_tmp59_ = _tmp58_;
			_tmp60_ = vala_code_node_get_source_reference ((ValaCodeNode*) _tmp59_);
			_tmp61_ = _tmp60_;
			vala_report_error (_tmp61_, "Expression must be constant");
			result = FALSE;
			_vala_code_node_unref0 (condition_target_type);
			_vala_code_node_unref0 (switch_statement);
			return result;
		}
		_tmp62_ = vala_switch_label_get_expression (self);
		_tmp63_ = _tmp62_;
		_tmp64_ = vala_expression_get_value_type (_tmp63_);
		_tmp65_ = _tmp64_;
		_tmp66_ = switch_statement;
		_tmp67_ = vala_switch_statement_get_expression (_tmp66_);
		_tmp68_ = _tmp67_;
		_tmp69_ = vala_expression_get_value_type (_tmp68_);
		_tmp70_ = _tmp69_;
		if (!vala_data_type_compatible (_tmp65_, _tmp70_)) {
			ValaExpression* _tmp71_;
			ValaExpression* _tmp72_;
			ValaSourceReference* _tmp73_;
			ValaSourceReference* _tmp74_;
			ValaExpression* _tmp75_;
			ValaExpression* _tmp76_;
			ValaDataType* _tmp77_;
			ValaDataType* _tmp78_;
			gchar* _tmp79_;
			gchar* _tmp80_;
			ValaSwitchStatement* _tmp81_;
			ValaExpression* _tmp82_;
			ValaExpression* _tmp83_;
			ValaDataType* _tmp84_;
			ValaDataType* _tmp85_;
			gchar* _tmp86_;
			gchar* _tmp87_;
			gchar* _tmp88_;
			gchar* _tmp89_;
			vala_code_node_set_error ((ValaCodeNode*) self, TRUE);
			_tmp71_ = vala_switch_label_get_expression (self);
			_tmp72_ = _tmp71_;
			_tmp73_ = vala_code_node_get_source_reference ((ValaCodeNode*) _tmp72_);
			_tmp74_ = _tmp73_;
			_tmp75_ = vala_switch_label_get_expression (self);
			_tmp76_ = _tmp75_;
			_tmp77_ = vala_expression_get_value_type (_tmp76_);
			_tmp78_ = _tmp77_;
			_tmp79_ = vala_code_node_to_string ((ValaCodeNode*) _tmp78_);
			_tmp80_ = _tmp79_;
			_tmp81_ = switch_statement;
			_tmp82_ = vala_switch_statement_get_expression (_tmp81_);
			_tmp83_ = _tmp82_;
			_tmp84_ = vala_expression_get_value_type (_tmp83_);
			_tmp85_ = _tmp84_;
			_tmp86_ = vala_code_node_to_string ((ValaCodeNode*) _tmp85_);
			_tmp87_ = _tmp86_;
			_tmp88_ = g_strdup_printf ("Cannot convert from `%s' to `%s'", _tmp80_, _tmp87_);
			_tmp89_ = _tmp88_;
			vala_report_error (_tmp74_, _tmp89_);
			_g_free0 (_tmp89_);
			_g_free0 (_tmp87_);
			_g_free0 (_tmp80_);
			result = FALSE;
			_vala_code_node_unref0 (condition_target_type);
			_vala_code_node_unref0 (switch_statement);
			return result;
		}
		_vala_code_node_unref0 (condition_target_type);
		_vala_code_node_unref0 (switch_statement);
	}
	result = TRUE;
	return result;
}

static void
vala_switch_label_real_emit (ValaCodeNode* base,
                             ValaCodeGenerator* codegen)
{
	ValaSwitchLabel * self;
	self = (ValaSwitchLabel*) base;
	g_return_if_fail (codegen != NULL);
	vala_code_visitor_visit_switch_label ((ValaCodeVisitor*) codegen, self);
}

ValaExpression*
vala_switch_label_get_expression (ValaSwitchLabel* self)
{
	ValaExpression* result;
	ValaExpression* _tmp0_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_expression;
	result = _tmp0_;
	return result;
}

void
vala_switch_label_set_expression (ValaSwitchLabel* self,
                                  ValaExpression* value)
{
	ValaExpression* _tmp0_;
	ValaExpression* _tmp1_;
	g_return_if_fail (self != NULL);
	_tmp0_ = _vala_code_node_ref0 (value);
	_vala_code_node_unref0 (self->priv->_expression);
	self->priv->_expression = _tmp0_;
	_tmp1_ = self->priv->_expression;
	vala_code_node_set_parent_node ((ValaCodeNode*) _tmp1_, (ValaCodeNode*) self);
}

ValaSwitchSection*
vala_switch_label_get_section (ValaSwitchLabel* self)
{
	ValaSwitchSection* result;
	ValaCodeNode* _tmp0_;
	ValaCodeNode* _tmp1_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = vala_code_node_get_parent_node ((ValaCodeNode*) self);
	_tmp1_ = _tmp0_;
	result = G_TYPE_CHECK_INSTANCE_CAST (_tmp1_, VALA_TYPE_SWITCH_SECTION, ValaSwitchSection);
	return result;
}

static void
vala_switch_label_class_init (ValaSwitchLabelClass * klass,
                              gpointer klass_data)
{
	vala_switch_label_parent_class = g_type_class_peek_parent (klass);
	((ValaCodeNodeClass *) klass)->finalize = vala_switch_label_finalize;
	g_type_class_adjust_private_offset (klass, &ValaSwitchLabel_private_offset);
	((ValaCodeNodeClass *) klass)->accept = (void (*) (ValaCodeNode*, ValaCodeVisitor*)) vala_switch_label_real_accept;
	((ValaCodeNodeClass *) klass)->accept_children = (void (*) (ValaCodeNode*, ValaCodeVisitor*)) vala_switch_label_real_accept_children;
	((ValaCodeNodeClass *) klass)->check = (gboolean (*) (ValaCodeNode*, ValaCodeContext*)) vala_switch_label_real_check;
	((ValaCodeNodeClass *) klass)->emit = (void (*) (ValaCodeNode*, ValaCodeGenerator*)) vala_switch_label_real_emit;
}

static void
vala_switch_label_instance_init (ValaSwitchLabel * self,
                                 gpointer klass)
{
	self->priv = vala_switch_label_get_instance_private (self);
}

static void
vala_switch_label_finalize (ValaCodeNode * obj)
{
	ValaSwitchLabel * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_SWITCH_LABEL, ValaSwitchLabel);
	_vala_code_node_unref0 (self->priv->_expression);
	VALA_CODE_NODE_CLASS (vala_switch_label_parent_class)->finalize (obj);
}

/**
 * Represents a switch label in the source code.
 */
GType
vala_switch_label_get_type (void)
{
	static volatile gsize vala_switch_label_type_id__volatile = 0;
	if (g_once_init_enter (&vala_switch_label_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ValaSwitchLabelClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) vala_switch_label_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ValaSwitchLabel), 0, (GInstanceInitFunc) vala_switch_label_instance_init, NULL };
		GType vala_switch_label_type_id;
		vala_switch_label_type_id = g_type_register_static (VALA_TYPE_CODE_NODE, "ValaSwitchLabel", &g_define_type_info, 0);
		ValaSwitchLabel_private_offset = g_type_add_instance_private (vala_switch_label_type_id, sizeof (ValaSwitchLabelPrivate));
		g_once_init_leave (&vala_switch_label_type_id__volatile, vala_switch_label_type_id);
	}
	return vala_switch_label_type_id__volatile;
}

