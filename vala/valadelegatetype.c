/* valadelegatetype.c generated by valac, the Vala compiler
 * generated from valadelegatetype.vala, do not modify */

/* valadelegatetype.vala
 *
 * Copyright (C) 2007-2012  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

#include "vala.h"
#include <glib.h>
#include <valagee.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>

#define _g_free0(var) (var = (g_free (var), NULL))
#define _vala_code_node_unref0(var) ((var == NULL) ? NULL : (var = (vala_code_node_unref (var), NULL)))
#define _vala_scope_unref0(var) ((var == NULL) ? NULL : (var = (vala_scope_unref (var), NULL)))
#define _vala_iterable_unref0(var) ((var == NULL) ? NULL : (var = (vala_iterable_unref (var), NULL)))
#define _vala_iterator_unref0(var) ((var == NULL) ? NULL : (var = (vala_iterator_unref (var), NULL)))

struct _ValaDelegateTypePrivate {
	ValaDelegate* _delegate_symbol;
	gboolean _is_called_once;
};

static gint ValaDelegateType_private_offset;
static gpointer vala_delegate_type_parent_class = NULL;

static gboolean vala_delegate_type_real_is_invokable (ValaDataType* base);
static ValaDataType* vala_delegate_type_real_get_return_type (ValaDataType* base);
static ValaList* vala_delegate_type_real_get_parameters (ValaDataType* base);
static gchar* vala_delegate_type_real_to_qualified_string (ValaDataType* base,
                                                    ValaScope* scope);
static ValaDataType* vala_delegate_type_real_copy (ValaDataType* base);
static gboolean vala_delegate_type_real_is_accessible (ValaDataType* base,
                                                ValaSymbol* sym);
static gboolean vala_delegate_type_real_check (ValaCodeNode* base,
                                        ValaCodeContext* context);
static gboolean vala_delegate_type_real_compatible (ValaDataType* base,
                                             ValaDataType* target_type);
static gboolean vala_delegate_type_real_is_disposable (ValaDataType* base);
static void vala_delegate_type_finalize (ValaCodeNode * obj);

static inline gpointer
vala_delegate_type_get_instance_private (ValaDelegateType* self)
{
	return G_STRUCT_MEMBER_P (self, ValaDelegateType_private_offset);
}

ValaDelegateType*
vala_delegate_type_construct (GType object_type,
                              ValaDelegate* delegate_symbol)
{
	ValaDelegateType* self = NULL;
	gchar* _tmp0_;
	gchar* _tmp1_;
	g_return_val_if_fail (delegate_symbol != NULL, NULL);
	self = (ValaDelegateType*) vala_callable_type_construct (object_type);
	vala_delegate_type_set_delegate_symbol (self, delegate_symbol);
	_tmp0_ = vala_code_node_get_attribute_string ((ValaCodeNode*) delegate_symbol, "CCode", "scope", NULL);
	_tmp1_ = _tmp0_;
	vala_delegate_type_set_is_called_once (self, g_strcmp0 (_tmp1_, "async") == 0);
	_g_free0 (_tmp1_);
	return self;
}

ValaDelegateType*
vala_delegate_type_new (ValaDelegate* delegate_symbol)
{
	return vala_delegate_type_construct (VALA_TYPE_DELEGATE_TYPE, delegate_symbol);
}

static gboolean
vala_delegate_type_real_is_invokable (ValaDataType* base)
{
	ValaDelegateType * self;
	gboolean result = FALSE;
	self = (ValaDelegateType*) base;
	result = TRUE;
	return result;
}

static gpointer
_vala_code_node_ref0 (gpointer self)
{
	return self ? vala_code_node_ref (self) : NULL;
}

static ValaDataType*
vala_delegate_type_real_get_return_type (ValaDataType* base)
{
	ValaDelegateType * self;
	ValaDelegate* _tmp0_;
	ValaDataType* _tmp1_;
	ValaDataType* _tmp2_;
	ValaDataType* _tmp3_;
	ValaDataType* result = NULL;
	self = (ValaDelegateType*) base;
	_tmp0_ = self->priv->_delegate_symbol;
	_tmp1_ = vala_callable_get_return_type ((ValaCallable*) _tmp0_);
	_tmp2_ = _tmp1_;
	_tmp3_ = _vala_code_node_ref0 (_tmp2_);
	result = _tmp3_;
	return result;
}

static ValaList*
vala_delegate_type_real_get_parameters (ValaDataType* base)
{
	ValaDelegateType * self;
	ValaDelegate* _tmp0_;
	ValaList* _tmp1_;
	ValaList* result = NULL;
	self = (ValaDelegateType*) base;
	_tmp0_ = self->priv->_delegate_symbol;
	_tmp1_ = vala_callable_get_parameters ((ValaCallable*) _tmp0_);
	result = _tmp1_;
	return result;
}

static gpointer
_vala_scope_ref0 (gpointer self)
{
	return self ? vala_scope_ref (self) : NULL;
}

static gpointer
_vala_iterable_ref0 (gpointer self)
{
	return self ? vala_iterable_ref (self) : NULL;
}

static gchar*
vala_delegate_type_real_to_qualified_string (ValaDataType* base,
                                             ValaScope* scope)
{
	ValaDelegateType * self;
	ValaSymbol* global_symbol = NULL;
	ValaDelegate* _tmp0_;
	ValaSymbol* _tmp1_;
	ValaSymbol* sym = NULL;
	ValaScope* parent_scope = NULL;
	ValaScope* _tmp15_;
	gchar* s = NULL;
	gboolean _tmp28_ = FALSE;
	ValaSymbol* _tmp29_;
	ValaList* type_args = NULL;
	ValaList* _tmp38_;
	ValaList* _tmp39_;
	gint _tmp40_;
	gint _tmp41_;
	gboolean _tmp63_;
	gboolean _tmp64_;
	gchar* result = NULL;
	self = (ValaDelegateType*) base;
	_tmp0_ = self->priv->_delegate_symbol;
	_tmp1_ = _vala_code_node_ref0 ((ValaSymbol*) _tmp0_);
	global_symbol = _tmp1_;
	while (TRUE) {
		gboolean _tmp2_ = FALSE;
		ValaSymbol* _tmp3_;
		ValaSymbol* _tmp4_;
		ValaSymbol* _tmp5_;
		ValaSymbol* _tmp11_;
		ValaSymbol* _tmp12_;
		ValaSymbol* _tmp13_;
		ValaSymbol* _tmp14_;
		_tmp3_ = global_symbol;
		_tmp4_ = vala_symbol_get_parent_symbol (_tmp3_);
		_tmp5_ = _tmp4_;
		if (_tmp5_ != NULL) {
			ValaSymbol* _tmp6_;
			ValaSymbol* _tmp7_;
			ValaSymbol* _tmp8_;
			const gchar* _tmp9_;
			const gchar* _tmp10_;
			_tmp6_ = global_symbol;
			_tmp7_ = vala_symbol_get_parent_symbol (_tmp6_);
			_tmp8_ = _tmp7_;
			_tmp9_ = vala_symbol_get_name (_tmp8_);
			_tmp10_ = _tmp9_;
			_tmp2_ = _tmp10_ != NULL;
		} else {
			_tmp2_ = FALSE;
		}
		if (!_tmp2_) {
			break;
		}
		_tmp11_ = global_symbol;
		_tmp12_ = vala_symbol_get_parent_symbol (_tmp11_);
		_tmp13_ = _tmp12_;
		_tmp14_ = _vala_code_node_ref0 (_tmp13_);
		_vala_code_node_unref0 (global_symbol);
		global_symbol = _tmp14_;
	}
	sym = NULL;
	_tmp15_ = _vala_scope_ref0 (scope);
	parent_scope = _tmp15_;
	while (TRUE) {
		gboolean _tmp16_ = FALSE;
		ValaSymbol* _tmp17_;
		ValaScope* _tmp19_;
		ValaSymbol* _tmp20_;
		const gchar* _tmp21_;
		const gchar* _tmp22_;
		ValaSymbol* _tmp23_;
		ValaScope* _tmp24_;
		ValaScope* _tmp25_;
		ValaScope* _tmp26_;
		ValaScope* _tmp27_;
		_tmp17_ = sym;
		if (_tmp17_ == NULL) {
			ValaScope* _tmp18_;
			_tmp18_ = parent_scope;
			_tmp16_ = _tmp18_ != NULL;
		} else {
			_tmp16_ = FALSE;
		}
		if (!_tmp16_) {
			break;
		}
		_tmp19_ = parent_scope;
		_tmp20_ = global_symbol;
		_tmp21_ = vala_symbol_get_name (_tmp20_);
		_tmp22_ = _tmp21_;
		_tmp23_ = vala_scope_lookup (_tmp19_, _tmp22_);
		_vala_code_node_unref0 (sym);
		sym = _tmp23_;
		_tmp24_ = parent_scope;
		_tmp25_ = vala_scope_get_parent_scope (_tmp24_);
		_tmp26_ = _tmp25_;
		_tmp27_ = _vala_scope_ref0 (_tmp26_);
		_vala_scope_unref0 (parent_scope);
		parent_scope = _tmp27_;
	}
	_tmp29_ = sym;
	if (_tmp29_ != NULL) {
		ValaSymbol* _tmp30_;
		ValaSymbol* _tmp31_;
		_tmp30_ = global_symbol;
		_tmp31_ = sym;
		_tmp28_ = _tmp30_ != _tmp31_;
	} else {
		_tmp28_ = FALSE;
	}
	if (_tmp28_) {
		ValaDelegate* _tmp32_;
		gchar* _tmp33_;
		gchar* _tmp34_;
		gchar* _tmp35_;
		_tmp32_ = self->priv->_delegate_symbol;
		_tmp33_ = vala_symbol_get_full_name ((ValaSymbol*) _tmp32_);
		_tmp34_ = _tmp33_;
		_tmp35_ = g_strconcat ("global::", _tmp34_, NULL);
		_g_free0 (s);
		s = _tmp35_;
		_g_free0 (_tmp34_);
	} else {
		ValaDelegate* _tmp36_;
		gchar* _tmp37_;
		_tmp36_ = self->priv->_delegate_symbol;
		_tmp37_ = vala_symbol_get_full_name ((ValaSymbol*) _tmp36_);
		_g_free0 (s);
		s = _tmp37_;
	}
	_tmp38_ = vala_data_type_get_type_arguments ((ValaDataType*) self);
	type_args = _tmp38_;
	_tmp39_ = type_args;
	_tmp40_ = vala_collection_get_size ((ValaCollection*) _tmp39_);
	_tmp41_ = _tmp40_;
	if (_tmp41_ > 0) {
		const gchar* _tmp42_;
		gchar* _tmp43_;
		gboolean first = FALSE;
		const gchar* _tmp61_;
		gchar* _tmp62_;
		_tmp42_ = s;
		_tmp43_ = g_strconcat (_tmp42_, "<", NULL);
		_g_free0 (s);
		s = _tmp43_;
		first = TRUE;
		{
			ValaList* _type_arg_list = NULL;
			ValaList* _tmp44_;
			ValaList* _tmp45_;
			gint _type_arg_size = 0;
			ValaList* _tmp46_;
			gint _tmp47_;
			gint _tmp48_;
			gint _type_arg_index = 0;
			_tmp44_ = type_args;
			_tmp45_ = _vala_iterable_ref0 (_tmp44_);
			_type_arg_list = _tmp45_;
			_tmp46_ = _type_arg_list;
			_tmp47_ = vala_collection_get_size ((ValaCollection*) _tmp46_);
			_tmp48_ = _tmp47_;
			_type_arg_size = _tmp48_;
			_type_arg_index = -1;
			while (TRUE) {
				ValaDataType* type_arg = NULL;
				ValaList* _tmp49_;
				gpointer _tmp50_;
				ValaDataType* _tmp53_;
				const gchar* _tmp56_;
				ValaDataType* _tmp57_;
				gchar* _tmp58_;
				gchar* _tmp59_;
				gchar* _tmp60_;
				_type_arg_index = _type_arg_index + 1;
				if (!(_type_arg_index < _type_arg_size)) {
					break;
				}
				_tmp49_ = _type_arg_list;
				_tmp50_ = vala_list_get (_tmp49_, _type_arg_index);
				type_arg = (ValaDataType*) _tmp50_;
				if (!first) {
					const gchar* _tmp51_;
					gchar* _tmp52_;
					_tmp51_ = s;
					_tmp52_ = g_strconcat (_tmp51_, ",", NULL);
					_g_free0 (s);
					s = _tmp52_;
				} else {
					first = FALSE;
				}
				_tmp53_ = type_arg;
				if (vala_data_type_is_weak (_tmp53_)) {
					const gchar* _tmp54_;
					gchar* _tmp55_;
					_tmp54_ = s;
					_tmp55_ = g_strconcat (_tmp54_, "weak ", NULL);
					_g_free0 (s);
					s = _tmp55_;
				}
				_tmp56_ = s;
				_tmp57_ = type_arg;
				_tmp58_ = vala_data_type_to_qualified_string (_tmp57_, scope);
				_tmp59_ = _tmp58_;
				_tmp60_ = g_strconcat (_tmp56_, _tmp59_, NULL);
				_g_free0 (s);
				s = _tmp60_;
				_g_free0 (_tmp59_);
				_vala_code_node_unref0 (type_arg);
			}
			_vala_iterable_unref0 (_type_arg_list);
		}
		_tmp61_ = s;
		_tmp62_ = g_strconcat (_tmp61_, ">", NULL);
		_g_free0 (s);
		s = _tmp62_;
	}
	_tmp63_ = vala_data_type_get_nullable ((ValaDataType*) self);
	_tmp64_ = _tmp63_;
	if (_tmp64_) {
		const gchar* _tmp65_;
		gchar* _tmp66_;
		_tmp65_ = s;
		_tmp66_ = g_strconcat (_tmp65_, "?", NULL);
		_g_free0 (s);
		s = _tmp66_;
	}
	result = s;
	_vala_iterable_unref0 (type_args);
	_vala_scope_unref0 (parent_scope);
	_vala_code_node_unref0 (sym);
	_vala_code_node_unref0 (global_symbol);
	return result;
}

static ValaDataType*
vala_delegate_type_real_copy (ValaDataType* base)
{
	ValaDelegateType * self;
	ValaDelegateType* _result_ = NULL;
	ValaDelegate* _tmp0_;
	ValaDelegateType* _tmp1_;
	ValaDelegateType* _tmp2_;
	ValaSourceReference* _tmp3_;
	ValaSourceReference* _tmp4_;
	ValaDelegateType* _tmp5_;
	gboolean _tmp6_;
	gboolean _tmp7_;
	ValaDelegateType* _tmp8_;
	gboolean _tmp9_;
	gboolean _tmp10_;
	ValaDelegateType* _tmp21_;
	gboolean _tmp22_;
	ValaDataType* result = NULL;
	self = (ValaDelegateType*) base;
	_tmp0_ = self->priv->_delegate_symbol;
	_tmp1_ = vala_delegate_type_new (_tmp0_);
	_result_ = _tmp1_;
	_tmp2_ = _result_;
	_tmp3_ = vala_code_node_get_source_reference ((ValaCodeNode*) self);
	_tmp4_ = _tmp3_;
	vala_code_node_set_source_reference ((ValaCodeNode*) _tmp2_, _tmp4_);
	_tmp5_ = _result_;
	_tmp6_ = vala_data_type_get_value_owned ((ValaDataType*) self);
	_tmp7_ = _tmp6_;
	vala_data_type_set_value_owned ((ValaDataType*) _tmp5_, _tmp7_);
	_tmp8_ = _result_;
	_tmp9_ = vala_data_type_get_nullable ((ValaDataType*) self);
	_tmp10_ = _tmp9_;
	vala_data_type_set_nullable ((ValaDataType*) _tmp8_, _tmp10_);
	{
		ValaList* _arg_list = NULL;
		ValaList* _tmp11_;
		gint _arg_size = 0;
		ValaList* _tmp12_;
		gint _tmp13_;
		gint _tmp14_;
		gint _arg_index = 0;
		_tmp11_ = vala_data_type_get_type_arguments ((ValaDataType*) self);
		_arg_list = _tmp11_;
		_tmp12_ = _arg_list;
		_tmp13_ = vala_collection_get_size ((ValaCollection*) _tmp12_);
		_tmp14_ = _tmp13_;
		_arg_size = _tmp14_;
		_arg_index = -1;
		while (TRUE) {
			ValaDataType* arg = NULL;
			ValaList* _tmp15_;
			gpointer _tmp16_;
			ValaDelegateType* _tmp17_;
			ValaDataType* _tmp18_;
			ValaDataType* _tmp19_;
			ValaDataType* _tmp20_;
			_arg_index = _arg_index + 1;
			if (!(_arg_index < _arg_size)) {
				break;
			}
			_tmp15_ = _arg_list;
			_tmp16_ = vala_list_get (_tmp15_, _arg_index);
			arg = (ValaDataType*) _tmp16_;
			_tmp17_ = _result_;
			_tmp18_ = arg;
			_tmp19_ = vala_data_type_copy (_tmp18_);
			_tmp20_ = _tmp19_;
			vala_data_type_add_type_argument ((ValaDataType*) _tmp17_, _tmp20_);
			_vala_code_node_unref0 (_tmp20_);
			_vala_code_node_unref0 (arg);
		}
		_vala_iterable_unref0 (_arg_list);
	}
	_tmp21_ = _result_;
	_tmp22_ = self->priv->_is_called_once;
	vala_delegate_type_set_is_called_once (_tmp21_, _tmp22_);
	result = (ValaDataType*) _result_;
	return result;
}

static gboolean
vala_delegate_type_real_is_accessible (ValaDataType* base,
                                       ValaSymbol* sym)
{
	ValaDelegateType * self;
	ValaDelegate* _tmp0_;
	gboolean result = FALSE;
	self = (ValaDelegateType*) base;
	g_return_val_if_fail (sym != NULL, FALSE);
	_tmp0_ = self->priv->_delegate_symbol;
	result = vala_symbol_is_accessible ((ValaSymbol*) _tmp0_, sym);
	return result;
}

static gboolean
vala_delegate_type_real_check (ValaCodeNode* base,
                               ValaCodeContext* context)
{
	ValaDelegateType * self;
	gboolean _tmp0_ = FALSE;
	gboolean _tmp1_;
	ValaDelegate* _tmp6_;
	gint n_type_params = 0;
	ValaDelegate* _tmp7_;
	ValaList* _tmp8_;
	ValaList* _tmp9_;
	gint _tmp10_;
	gint _tmp11_;
	gint _tmp12_;
	gint n_type_args = 0;
	ValaList* _tmp13_;
	ValaList* _tmp14_;
	gint _tmp15_;
	gint _tmp16_;
	gint _tmp17_;
	gboolean _tmp18_ = FALSE;
	gboolean result = FALSE;
	self = (ValaDelegateType*) base;
	g_return_val_if_fail (context != NULL, FALSE);
	_tmp1_ = self->priv->_is_called_once;
	if (_tmp1_) {
		gboolean _tmp2_;
		gboolean _tmp3_;
		_tmp2_ = vala_data_type_get_value_owned ((ValaDataType*) self);
		_tmp3_ = _tmp2_;
		_tmp0_ = !_tmp3_;
	} else {
		_tmp0_ = FALSE;
	}
	if (_tmp0_) {
		ValaSourceReference* _tmp4_;
		ValaSourceReference* _tmp5_;
		_tmp4_ = vala_code_node_get_source_reference ((ValaCodeNode*) self);
		_tmp5_ = _tmp4_;
		vala_report_warning (_tmp5_, "delegates with scope=\"async\" must be owned");
	}
	_tmp6_ = self->priv->_delegate_symbol;
	if (!vala_code_node_check ((ValaCodeNode*) _tmp6_, context)) {
		result = FALSE;
		return result;
	}
	_tmp7_ = self->priv->_delegate_symbol;
	_tmp8_ = vala_delegate_get_type_parameters (_tmp7_);
	_tmp9_ = _tmp8_;
	_tmp10_ = vala_collection_get_size ((ValaCollection*) _tmp9_);
	_tmp11_ = _tmp10_;
	_tmp12_ = _tmp11_;
	_vala_iterable_unref0 (_tmp9_);
	n_type_params = _tmp12_;
	_tmp13_ = vala_data_type_get_type_arguments ((ValaDataType*) self);
	_tmp14_ = _tmp13_;
	_tmp15_ = vala_collection_get_size ((ValaCollection*) _tmp14_);
	_tmp16_ = _tmp15_;
	_tmp17_ = _tmp16_;
	_vala_iterable_unref0 (_tmp14_);
	n_type_args = _tmp17_;
	if (n_type_args > 0) {
		_tmp18_ = n_type_args < n_type_params;
	} else {
		_tmp18_ = FALSE;
	}
	if (_tmp18_) {
		ValaSourceReference* _tmp19_;
		ValaSourceReference* _tmp20_;
		_tmp19_ = vala_code_node_get_source_reference ((ValaCodeNode*) self);
		_tmp20_ = _tmp19_;
		vala_report_error (_tmp20_, "too few type arguments");
		result = FALSE;
		return result;
	} else {
		gboolean _tmp21_ = FALSE;
		if (n_type_args > 0) {
			_tmp21_ = n_type_args > n_type_params;
		} else {
			_tmp21_ = FALSE;
		}
		if (_tmp21_) {
			ValaSourceReference* _tmp22_;
			ValaSourceReference* _tmp23_;
			_tmp22_ = vala_code_node_get_source_reference ((ValaCodeNode*) self);
			_tmp23_ = _tmp22_;
			vala_report_error (_tmp23_, "too many type arguments");
			result = FALSE;
			return result;
		}
	}
	{
		ValaList* _type_list = NULL;
		ValaList* _tmp24_;
		gint _type_size = 0;
		ValaList* _tmp25_;
		gint _tmp26_;
		gint _tmp27_;
		gint _type_index = 0;
		_tmp24_ = vala_data_type_get_type_arguments ((ValaDataType*) self);
		_type_list = _tmp24_;
		_tmp25_ = _type_list;
		_tmp26_ = vala_collection_get_size ((ValaCollection*) _tmp25_);
		_tmp27_ = _tmp26_;
		_type_size = _tmp27_;
		_type_index = -1;
		while (TRUE) {
			ValaDataType* type = NULL;
			ValaList* _tmp28_;
			gpointer _tmp29_;
			ValaDataType* _tmp30_;
			_type_index = _type_index + 1;
			if (!(_type_index < _type_size)) {
				break;
			}
			_tmp28_ = _type_list;
			_tmp29_ = vala_list_get (_tmp28_, _type_index);
			type = (ValaDataType*) _tmp29_;
			_tmp30_ = type;
			if (!vala_code_node_check ((ValaCodeNode*) _tmp30_, context)) {
				result = FALSE;
				_vala_code_node_unref0 (type);
				_vala_iterable_unref0 (_type_list);
				return result;
			}
			_vala_code_node_unref0 (type);
		}
		_vala_iterable_unref0 (_type_list);
	}
	result = TRUE;
	return result;
}

static gboolean
vala_delegate_type_real_compatible (ValaDataType* base,
                                    ValaDataType* target_type)
{
	ValaDelegateType * self;
	ValaDelegateType* dt_target = NULL;
	ValaDelegateType* _tmp0_;
	ValaDelegateType* _tmp1_;
	ValaDelegate* _tmp2_;
	ValaDelegateType* _tmp3_;
	ValaDelegate* _tmp4_;
	ValaDataType* _tmp5_;
	ValaDataType* _tmp6_;
	ValaDelegateType* _tmp7_;
	ValaDataType* _tmp8_;
	ValaDataType* _tmp9_;
	ValaDelegateType* _tmp10_;
	ValaDataType* _tmp11_;
	ValaDataType* _tmp12_;
	gboolean _tmp13_;
	ValaList* parameters = NULL;
	ValaList* _tmp14_;
	ValaIterator* params_it = NULL;
	ValaList* _tmp15_;
	ValaIterator* _tmp16_;
	gboolean _tmp17_ = FALSE;
	gboolean _tmp18_ = FALSE;
	ValaDelegateType* _tmp19_;
	ValaDelegate* _tmp20_;
	ValaSymbol* _tmp21_;
	ValaSymbol* _tmp22_;
	ValaIterator* _tmp64_;
	ValaArrayList* error_types = NULL;
	GEqualFunc _tmp65_;
	ValaArrayList* _tmp66_;
	ValaArrayList* _tmp67_;
	gboolean result = FALSE;
	self = (ValaDelegateType*) base;
	g_return_val_if_fail (target_type != NULL, FALSE);
	_tmp0_ = _vala_code_node_ref0 (VALA_IS_DELEGATE_TYPE (target_type) ? ((ValaDelegateType*) target_type) : NULL);
	dt_target = _tmp0_;
	_tmp1_ = dt_target;
	if (_tmp1_ == NULL) {
		result = FALSE;
		_vala_code_node_unref0 (dt_target);
		return result;
	}
	_tmp2_ = self->priv->_delegate_symbol;
	_tmp3_ = dt_target;
	_tmp4_ = _tmp3_->priv->_delegate_symbol;
	if (_tmp2_ == _tmp4_) {
		result = TRUE;
		_vala_code_node_unref0 (dt_target);
		return result;
	}
	_tmp5_ = vala_data_type_get_return_type ((ValaDataType*) self);
	_tmp6_ = _tmp5_;
	_tmp7_ = dt_target;
	_tmp8_ = vala_data_type_get_return_type ((ValaDataType*) _tmp7_);
	_tmp9_ = _tmp8_;
	_tmp10_ = dt_target;
	_tmp11_ = vala_data_type_get_actual_type (_tmp9_, (ValaDataType*) _tmp10_, NULL, (ValaCodeNode*) self);
	_tmp12_ = _tmp11_;
	_tmp13_ = !vala_data_type_stricter (_tmp6_, _tmp12_);
	_vala_code_node_unref0 (_tmp12_);
	_vala_code_node_unref0 (_tmp9_);
	_vala_code_node_unref0 (_tmp6_);
	if (_tmp13_) {
		result = FALSE;
		_vala_code_node_unref0 (dt_target);
		return result;
	}
	_tmp14_ = vala_data_type_get_parameters ((ValaDataType*) self);
	parameters = _tmp14_;
	_tmp15_ = parameters;
	_tmp16_ = vala_iterable_iterator ((ValaIterable*) _tmp15_);
	params_it = _tmp16_;
	_tmp19_ = dt_target;
	_tmp20_ = _tmp19_->priv->_delegate_symbol;
	_tmp21_ = vala_symbol_get_parent_symbol ((ValaSymbol*) _tmp20_);
	_tmp22_ = _tmp21_;
	if (VALA_IS_SIGNAL (_tmp22_)) {
		ValaDelegateType* _tmp23_;
		ValaDelegate* _tmp24_;
		ValaDataType* _tmp25_;
		ValaDataType* _tmp26_;
		_tmp23_ = dt_target;
		_tmp24_ = _tmp23_->priv->_delegate_symbol;
		_tmp25_ = vala_delegate_get_sender_type (_tmp24_);
		_tmp26_ = _tmp25_;
		_tmp18_ = _tmp26_ != NULL;
	} else {
		_tmp18_ = FALSE;
	}
	if (_tmp18_) {
		ValaList* _tmp27_;
		gint _tmp28_;
		gint _tmp29_;
		ValaDelegateType* _tmp30_;
		ValaList* _tmp31_;
		ValaList* _tmp32_;
		gint _tmp33_;
		gint _tmp34_;
		_tmp27_ = parameters;
		_tmp28_ = vala_collection_get_size ((ValaCollection*) _tmp27_);
		_tmp29_ = _tmp28_;
		_tmp30_ = dt_target;
		_tmp31_ = vala_data_type_get_parameters ((ValaDataType*) _tmp30_);
		_tmp32_ = _tmp31_;
		_tmp33_ = vala_collection_get_size ((ValaCollection*) _tmp32_);
		_tmp34_ = _tmp33_;
		_tmp17_ = _tmp29_ == (_tmp34_ + 1);
		_vala_iterable_unref0 (_tmp32_);
	} else {
		_tmp17_ = FALSE;
	}
	if (_tmp17_) {
		ValaIterator* _tmp35_;
		ValaParameter* p = NULL;
		ValaIterator* _tmp36_;
		gpointer _tmp37_;
		ValaDelegateType* _tmp38_;
		ValaDelegate* _tmp39_;
		ValaDataType* _tmp40_;
		ValaDataType* _tmp41_;
		ValaParameter* _tmp42_;
		ValaDataType* _tmp43_;
		ValaDataType* _tmp44_;
		_tmp35_ = params_it;
		vala_iterator_next (_tmp35_);
		_tmp36_ = params_it;
		_tmp37_ = vala_iterator_get (_tmp36_);
		p = (ValaParameter*) _tmp37_;
		_tmp38_ = dt_target;
		_tmp39_ = _tmp38_->priv->_delegate_symbol;
		_tmp40_ = vala_delegate_get_sender_type (_tmp39_);
		_tmp41_ = _tmp40_;
		_tmp42_ = p;
		_tmp43_ = vala_variable_get_variable_type ((ValaVariable*) _tmp42_);
		_tmp44_ = _tmp43_;
		if (!vala_data_type_stricter (_tmp41_, _tmp44_)) {
			result = FALSE;
			_vala_code_node_unref0 (p);
			_vala_iterator_unref0 (params_it);
			_vala_iterable_unref0 (parameters);
			_vala_code_node_unref0 (dt_target);
			return result;
		}
		_vala_code_node_unref0 (p);
	}
	{
		ValaList* _param_list = NULL;
		ValaDelegateType* _tmp45_;
		ValaList* _tmp46_;
		gint _param_size = 0;
		ValaList* _tmp47_;
		gint _tmp48_;
		gint _tmp49_;
		gint _param_index = 0;
		_tmp45_ = dt_target;
		_tmp46_ = vala_data_type_get_parameters ((ValaDataType*) _tmp45_);
		_param_list = _tmp46_;
		_tmp47_ = _param_list;
		_tmp48_ = vala_collection_get_size ((ValaCollection*) _tmp47_);
		_tmp49_ = _tmp48_;
		_param_size = _tmp49_;
		_param_index = -1;
		while (TRUE) {
			ValaParameter* param = NULL;
			ValaList* _tmp50_;
			gpointer _tmp51_;
			ValaIterator* _tmp52_;
			ValaParameter* p = NULL;
			ValaIterator* _tmp53_;
			gpointer _tmp54_;
			ValaParameter* _tmp55_;
			ValaDataType* _tmp56_;
			ValaDataType* _tmp57_;
			ValaDataType* _tmp58_;
			ValaDataType* _tmp59_;
			ValaParameter* _tmp60_;
			ValaDataType* _tmp61_;
			ValaDataType* _tmp62_;
			gboolean _tmp63_;
			_param_index = _param_index + 1;
			if (!(_param_index < _param_size)) {
				break;
			}
			_tmp50_ = _param_list;
			_tmp51_ = vala_list_get (_tmp50_, _param_index);
			param = (ValaParameter*) _tmp51_;
			_tmp52_ = params_it;
			if (!vala_iterator_next (_tmp52_)) {
				_vala_code_node_unref0 (param);
				break;
			}
			_tmp53_ = params_it;
			_tmp54_ = vala_iterator_get (_tmp53_);
			p = (ValaParameter*) _tmp54_;
			_tmp55_ = param;
			_tmp56_ = vala_variable_get_variable_type ((ValaVariable*) _tmp55_);
			_tmp57_ = _tmp56_;
			_tmp58_ = vala_data_type_get_actual_type (_tmp57_, (ValaDataType*) self, NULL, (ValaCodeNode*) self);
			_tmp59_ = _tmp58_;
			_tmp60_ = p;
			_tmp61_ = vala_variable_get_variable_type ((ValaVariable*) _tmp60_);
			_tmp62_ = _tmp61_;
			_tmp63_ = !vala_data_type_stricter (_tmp59_, _tmp62_);
			_vala_code_node_unref0 (_tmp59_);
			if (_tmp63_) {
				result = FALSE;
				_vala_code_node_unref0 (p);
				_vala_code_node_unref0 (param);
				_vala_iterable_unref0 (_param_list);
				_vala_iterator_unref0 (params_it);
				_vala_iterable_unref0 (parameters);
				_vala_code_node_unref0 (dt_target);
				return result;
			}
			_vala_code_node_unref0 (p);
			_vala_code_node_unref0 (param);
		}
		_vala_iterable_unref0 (_param_list);
	}
	_tmp64_ = params_it;
	if (vala_iterator_next (_tmp64_)) {
		result = FALSE;
		_vala_iterator_unref0 (params_it);
		_vala_iterable_unref0 (parameters);
		_vala_code_node_unref0 (dt_target);
		return result;
	}
	_tmp65_ = g_direct_equal;
	_tmp66_ = vala_array_list_new (VALA_TYPE_DATA_TYPE, (GBoxedCopyFunc) vala_code_node_ref, (GDestroyNotify) vala_code_node_unref, _tmp65_);
	error_types = _tmp66_;
	_tmp67_ = error_types;
	vala_code_node_get_error_types ((ValaCodeNode*) self, (ValaCollection*) _tmp67_, NULL);
	{
		ValaArrayList* _error_type_list = NULL;
		ValaArrayList* _tmp68_;
		ValaArrayList* _tmp69_;
		gint _error_type_size = 0;
		ValaArrayList* _tmp70_;
		gint _tmp71_;
		gint _tmp72_;
		gint _error_type_index = 0;
		_tmp68_ = error_types;
		_tmp69_ = _vala_iterable_ref0 (_tmp68_);
		_error_type_list = _tmp69_;
		_tmp70_ = _error_type_list;
		_tmp71_ = vala_collection_get_size ((ValaCollection*) _tmp70_);
		_tmp72_ = _tmp71_;
		_error_type_size = _tmp72_;
		_error_type_index = -1;
		while (TRUE) {
			ValaDataType* error_type = NULL;
			ValaArrayList* _tmp73_;
			gpointer _tmp74_;
			gboolean match = FALSE;
			ValaArrayList* delegate_error_types = NULL;
			GEqualFunc _tmp75_;
			ValaArrayList* _tmp76_;
			ValaDelegateType* _tmp77_;
			ValaArrayList* _tmp78_;
			_error_type_index = _error_type_index + 1;
			if (!(_error_type_index < _error_type_size)) {
				break;
			}
			_tmp73_ = _error_type_list;
			_tmp74_ = vala_list_get ((ValaList*) _tmp73_, _error_type_index);
			error_type = (ValaDataType*) _tmp74_;
			match = FALSE;
			_tmp75_ = g_direct_equal;
			_tmp76_ = vala_array_list_new (VALA_TYPE_DATA_TYPE, (GBoxedCopyFunc) vala_code_node_ref, (GDestroyNotify) vala_code_node_unref, _tmp75_);
			delegate_error_types = _tmp76_;
			_tmp77_ = dt_target;
			_tmp78_ = delegate_error_types;
			vala_code_node_get_error_types ((ValaCodeNode*) _tmp77_, (ValaCollection*) _tmp78_, NULL);
			{
				ValaArrayList* _delegate_error_type_list = NULL;
				ValaArrayList* _tmp79_;
				ValaArrayList* _tmp80_;
				gint _delegate_error_type_size = 0;
				ValaArrayList* _tmp81_;
				gint _tmp82_;
				gint _tmp83_;
				gint _delegate_error_type_index = 0;
				_tmp79_ = delegate_error_types;
				_tmp80_ = _vala_iterable_ref0 (_tmp79_);
				_delegate_error_type_list = _tmp80_;
				_tmp81_ = _delegate_error_type_list;
				_tmp82_ = vala_collection_get_size ((ValaCollection*) _tmp81_);
				_tmp83_ = _tmp82_;
				_delegate_error_type_size = _tmp83_;
				_delegate_error_type_index = -1;
				while (TRUE) {
					ValaDataType* delegate_error_type = NULL;
					ValaArrayList* _tmp84_;
					gpointer _tmp85_;
					ValaDataType* _tmp86_;
					ValaDataType* _tmp87_;
					_delegate_error_type_index = _delegate_error_type_index + 1;
					if (!(_delegate_error_type_index < _delegate_error_type_size)) {
						break;
					}
					_tmp84_ = _delegate_error_type_list;
					_tmp85_ = vala_list_get ((ValaList*) _tmp84_, _delegate_error_type_index);
					delegate_error_type = (ValaDataType*) _tmp85_;
					_tmp86_ = error_type;
					_tmp87_ = delegate_error_type;
					if (vala_data_type_compatible (_tmp86_, _tmp87_)) {
						match = TRUE;
						_vala_code_node_unref0 (delegate_error_type);
						break;
					}
					_vala_code_node_unref0 (delegate_error_type);
				}
				_vala_iterable_unref0 (_delegate_error_type_list);
			}
			if (!match) {
				result = FALSE;
				_vala_iterable_unref0 (delegate_error_types);
				_vala_code_node_unref0 (error_type);
				_vala_iterable_unref0 (_error_type_list);
				_vala_iterable_unref0 (error_types);
				_vala_iterator_unref0 (params_it);
				_vala_iterable_unref0 (parameters);
				_vala_code_node_unref0 (dt_target);
				return result;
			}
			_vala_iterable_unref0 (delegate_error_types);
			_vala_code_node_unref0 (error_type);
		}
		_vala_iterable_unref0 (_error_type_list);
	}
	result = TRUE;
	_vala_iterable_unref0 (error_types);
	_vala_iterator_unref0 (params_it);
	_vala_iterable_unref0 (parameters);
	_vala_code_node_unref0 (dt_target);
	return result;
}

static gboolean
vala_delegate_type_real_is_disposable (ValaDataType* base)
{
	ValaDelegateType * self;
	gboolean _tmp0_ = FALSE;
	gboolean _tmp1_ = FALSE;
	ValaDelegate* _tmp2_;
	gboolean _tmp3_;
	gboolean _tmp4_;
	gboolean result = FALSE;
	self = (ValaDelegateType*) base;
	_tmp2_ = self->priv->_delegate_symbol;
	_tmp3_ = vala_delegate_get_has_target (_tmp2_);
	_tmp4_ = _tmp3_;
	if (_tmp4_) {
		gboolean _tmp5_;
		gboolean _tmp6_;
		_tmp5_ = vala_data_type_get_value_owned ((ValaDataType*) self);
		_tmp6_ = _tmp5_;
		_tmp1_ = _tmp6_;
	} else {
		_tmp1_ = FALSE;
	}
	if (_tmp1_) {
		gboolean _tmp7_;
		_tmp7_ = self->priv->_is_called_once;
		_tmp0_ = !_tmp7_;
	} else {
		_tmp0_ = FALSE;
	}
	result = _tmp0_;
	return result;
}

ValaDelegate*
vala_delegate_type_get_delegate_symbol (ValaDelegateType* self)
{
	ValaDelegate* result;
	ValaDelegate* _tmp0_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_delegate_symbol;
	result = _tmp0_;
	return result;
}

void
vala_delegate_type_set_delegate_symbol (ValaDelegateType* self,
                                        ValaDelegate* value)
{
	g_return_if_fail (self != NULL);
	self->priv->_delegate_symbol = value;
}

gboolean
vala_delegate_type_get_is_called_once (ValaDelegateType* self)
{
	gboolean result;
	g_return_val_if_fail (self != NULL, FALSE);
	result = self->priv->_is_called_once;
	return result;
}

void
vala_delegate_type_set_is_called_once (ValaDelegateType* self,
                                       gboolean value)
{
	g_return_if_fail (self != NULL);
	self->priv->_is_called_once = value;
}

static void
vala_delegate_type_class_init (ValaDelegateTypeClass * klass,
                               gpointer klass_data)
{
	vala_delegate_type_parent_class = g_type_class_peek_parent (klass);
	((ValaCodeNodeClass *) klass)->finalize = vala_delegate_type_finalize;
	g_type_class_adjust_private_offset (klass, &ValaDelegateType_private_offset);
	((ValaDataTypeClass *) klass)->is_invokable = (gboolean (*) (ValaDataType*)) vala_delegate_type_real_is_invokable;
	((ValaDataTypeClass *) klass)->get_return_type = (ValaDataType* (*) (ValaDataType*)) vala_delegate_type_real_get_return_type;
	((ValaDataTypeClass *) klass)->get_parameters = (ValaList* (*) (ValaDataType*)) vala_delegate_type_real_get_parameters;
	((ValaDataTypeClass *) klass)->to_qualified_string = (gchar* (*) (ValaDataType*, ValaScope*)) vala_delegate_type_real_to_qualified_string;
	((ValaDataTypeClass *) klass)->copy = (ValaDataType* (*) (ValaDataType*)) vala_delegate_type_real_copy;
	((ValaDataTypeClass *) klass)->is_accessible = (gboolean (*) (ValaDataType*, ValaSymbol*)) vala_delegate_type_real_is_accessible;
	((ValaCodeNodeClass *) klass)->check = (gboolean (*) (ValaCodeNode*, ValaCodeContext*)) vala_delegate_type_real_check;
	((ValaDataTypeClass *) klass)->compatible = (gboolean (*) (ValaDataType*, ValaDataType*)) vala_delegate_type_real_compatible;
	((ValaDataTypeClass *) klass)->is_disposable = (gboolean (*) (ValaDataType*)) vala_delegate_type_real_is_disposable;
}

static void
vala_delegate_type_instance_init (ValaDelegateType * self,
                                  gpointer klass)
{
	self->priv = vala_delegate_type_get_instance_private (self);
}

static void
vala_delegate_type_finalize (ValaCodeNode * obj)
{
	ValaDelegateType * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, VALA_TYPE_DELEGATE_TYPE, ValaDelegateType);
	VALA_CODE_NODE_CLASS (vala_delegate_type_parent_class)->finalize (obj);
}

/**
 * The type of an instance of a delegate.
 */
GType
vala_delegate_type_get_type (void)
{
	static volatile gsize vala_delegate_type_type_id__volatile = 0;
	if (g_once_init_enter (&vala_delegate_type_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ValaDelegateTypeClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) vala_delegate_type_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ValaDelegateType), 0, (GInstanceInitFunc) vala_delegate_type_instance_init, NULL };
		GType vala_delegate_type_type_id;
		vala_delegate_type_type_id = g_type_register_static (VALA_TYPE_CALLABLE_TYPE, "ValaDelegateType", &g_define_type_info, 0);
		ValaDelegateType_private_offset = g_type_add_instance_private (vala_delegate_type_type_id, sizeof (ValaDelegateTypePrivate));
		g_once_init_leave (&vala_delegate_type_type_id__volatile, vala_delegate_type_type_id);
	}
	return vala_delegate_type_type_id__volatile;
}

